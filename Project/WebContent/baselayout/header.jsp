<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<head>
<meta charset="UTF-8">
<title>header</title>
<link rel="stylesheet"href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"crossorigin="anonymous">
</head>
<body>
<nav class="navbar navbar-expand-sm navbar-light bg-light">
	<div class="col-sm-11 mx-auto row">
		<a class="navbar-brand" href="Index">
			<img class=""src="images/logo.png" alt="" width="50" height="">IWATE
		</a>

		<div class="collapse navbar-collapse justify-content-end" id="navbarNav">
			<ul class="navbar-nav">
			<% boolean isLogin = session.getAttribute("isLogin")!=null?(boolean) session.getAttribute("isLogin"):false; %>

				<%if(isLogin){ %>
					<% int userId = (int) session.getAttribute("userId"); %>
					<%if(userId == 1){ %>
						<li class="nav-item"><a class="nav-link" href="ItemMaster"><button type="button" class="btn btn-outline-success mb-4">商品マスタ</button></a></li>
					<%} %>
				<%}%>

				<%if(isLogin){ %>
				<li class="nav-item"><a class="nav-link" rel="tooltip" title="ユーザー情報" href="User"><img class="" src="images/user.png" alt="" width="30" height=""></a></li>
				<%}else{%>
				<li class="nav-item"><a class="nav-link" href="Regist"><button type="button" class="btn btn-outline-danger mb-4">新規登録</button></a></li>
				<%}%>

				<li class="nav-item"><a class="nav-link" href="Cart"><img class="" src="images/cart.png" alt="" width="30" height=""></a></li>

				<%if(isLogin){ %>
				<li class="nav-item"><a class="nav-link" href="Logout"><img class="" src="images/login.png" alt="" width="30" height=""></a></li>
				<%}else{%>
				<li class="nav-item"><a class="nav-link" href="Login"><button type="button" class="btn btn-outline-success mb-4">ログイン</button></a>
				</li>
				<%}%>
			</ul>
		</div>
	</div>
</nav>
</body>
