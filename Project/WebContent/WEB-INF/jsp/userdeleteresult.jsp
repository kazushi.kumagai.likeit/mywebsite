<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>userdereteresult</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />

	<div class="text-center col-sm-9 mt-sm-4 mx-auto">
		<h2 class="my-sm-4">退会処理を完了しました</h2>
		<p>ご利用いただきましてありがとうございました。</p>
		<a href="Index"><button type="button" class="col-sm-5 btn btn-dark">トップページへ</button></a>
	</div>

	<jsp:include page="/baselayout/footer_bottom.jsp" />
</body>
</html>