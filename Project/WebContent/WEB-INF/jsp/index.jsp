<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>index</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
<link href="/MyWebSite/css/product.css" rel="stylesheet">
<link href="/MyWebSite/css/overlay.css" rel="stylesheet">

</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />

	<div
		class="position-relative overflow-hidden m-md-3 text-center bg-light">
		<div class="bg-light mr-md-3 text-center overflow-hidden">
			<div class="card">
				<img class="card-img" src="images/top1.jpg" alt="" width="100%"
					height="450">
				<div class="card-img-overlay col-md-5 p-lg-5 mx-auto my-5">
					<h1 class="card-title display-4 font-weight-normal text-white">IWATE</h1>
					<p class="card-text lead font-weight-normal text-white">岩手県の恵まれた自然が育んだ安全でおいしい特産品をご紹介</p>
				</div>
			</div>
		</div>
	</div>

	<div class="m-md-3">
		<div class="card-boby bg-light p-lg-3 mx-auto row">
			<form class="form-inlinecol col-sm-8 mx-auto row" action="ItemSearchResult">
				<div class="form-group col-sm-3">
					<select class="form-control" name="category">
						<option selected value="">カテゴリ</option>
						<option>お菓子</option>
						<option>海産物</option>
						<option>農産物</option>
						<option>盛岡冷麺・麺類</option>
						<option>お酒・飲料</option>
						<option>お弁当・パン</option>
						<option>調味料・お漬物</option>
						<option>工芸品</option>
					</select>
				</div>

				<div class="form-group col-sm-4 mb-2">
					<label class="sr-only">キーワード</label>
					<input type="text"class="form-control" placeholder="キーワード検索" name="name">
				</div>
				<button type="submit" class="btn btn-outline-success mb-4 ml-sm-5">Search</button>
			</form>
		</div>
	</div>

	<div class="d-md-flex flex-md-equal w-100 my-md-3 pl-md-3">
		<div class="bg-light mr-md-3 text-center overflow-hidden">
		<a href="ItemSearchResult?category=お菓子">
			<div class="card">
				<img class="card-img" src="images/okashi.jpg" alt="" width="100%"
					height="100%">
				<div class="card-img-overlay col-md-12 p-lg-5 mx-auto my-5">
					<h2 class="card-title text-white">お菓子</h2>
					<p class="card-text text-white"></p>
				</div>
			</div>
			</a>
		</div>
		<div class="bg-light mr-md-3 text-center overflow-hidden">
			<a href="ItemSearchResult?category=海産物">
			<div class="card">
				<img class="card-img" src="images/kaisan.jpg" alt="" width="100%"
					height="100%">
				<div class="card-img-overlay col-md-12 p-lg-5 mx-auto my-5">
					<h2 class="card-title text-white">海産物</h2>
					<p class="card-text text-white"></p>
				</div>
			</div>
			</a>
		</div>
	</div>

	<div class="d-md-flex flex-md-equal w-100 my-md-3 pl-md-3">
		<div class="bg-light mr-md-3 text-center overflow-hidden">
		<a href="ItemSearchResult?category=農産物">
			<div class="card">
				<img class="card-img" src="images/niku.jpg" alt="" width="100%"
					height="100%">
				<div class="card-img-overlay col-md-12 p-lg-5 mx-auto my-5">
					<h2 class="card-title text-white">農産物</h2>
					<p class="card-text text-white"></p>
				</div>
			</div>
			</a>
		</div>
		<div class="bg-light mr-md-3 text-center overflow-hidden">
		<a href="ItemSearchResult?category=盛岡冷麺・麺類">
			<div class="card">
				<img class="card-img" src="images/menn.jpg" alt="" width="100%"
					height="100%">
				<div class="card-img-overlay col-md-12 p-lg-5 mx-auto my-5">
					<h2 class="card-title text-white">盛岡冷麺・麺類</h2>
					<p class="card-text text-white"></p>
				</div>
			</div>
			</a>
		</div>
	</div>

	<div class="d-md-flex flex-md-equal w-100 my-md-3 pl-md-3">
		<div class="bg-light mr-md-3 text-center overflow-hidden">
		<a href="ItemSearchResult?category=お酒・飲料">
			<div class="card">
				<img class="card-img" src="images/jurce.jpg" alt="" width="100%"
					height="100%">
				<div class="card-img-overlay col-md-12 p-lg-5 mx-auto my-5">
					<h2 class="card-title text-white">お酒・飲料</h2>
					<p class="card-text text-white"></p>
				</div>
			</div>
			</a>
		</div>
		<div class="bg-light mr-md-3 text-center overflow-hidden">
		<a href="ItemSearchResult?category=お弁当・パン">
			<div class="card">
				<img class="card-img" src="images/benntou.jpg" alt="" width="100%"
					height="100%">
				<div class="card-img-overlay col-md-12 p-lg-5 mx-auto my-5">
					<h2 class="card-title text-white">お弁当・パン</h2>
					<p class="card-text text-white"></p>
				</div>
			</div>
			</a>
		</div>
	</div>

	<div class="d-md-flex flex-md-equal w-100 my-md-3 pl-md-3">
		<div class="bg-light mr-md-3 text-center overflow-hidden">
		<a href="ItemSearchResult?category=調味料・お漬物">
			<div class="card">
				<img class="card-img" src="images/tsukemono.jpg" alt="" width="100%"
					height="100%">
				<div class="card-img-overlay col-md-12 p-lg-5 mx-auto my-5">
					<h2 class="card-title text-white">調味料・お漬物</h2>
					<p class="card-text text-white"></p>
				</div>
			</div>
			</a>
		</div>
		<div class="bg-light mr-md-3 text-center overflow-hidden">
		<a href="ItemSearchResult?category=工芸品">
			<div class="card">
				<img class="card-img" src="images/kougeihinn.jpg" alt=""
					width="100%" height="">
				<div class="card-img-overlay col-md-12 p-lg-5 mx-auto my-5">
					<h2 class="card-title text-white">工芸品</h2>
					<p class="card-text text-white"></p>
				</div>
			</div>
			</a>
		</div>
	</div>
	<br>
	<jsp:include page="/baselayout/footer.jsp" />


</body>
</html>