<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>regist</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />

	<div class="col-sm-11 mx-auto">
		<h3>会員登録</h3>
	</div>

	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>
	<br>

	<form action="Regist" method="post">
		<div class="card col-sm-8 mx-auto">
			<div class="card-header">個人情報</div>
			<div class="card-body">
				<div class="form-group col-sm-12 mx-auto row">
					<label class="col-sm-4 col-form-label">名前</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" name="name" value="${strName}" required>
					</div>
				</div>
				<div class="form-group col-sm-12 mx-auto row">
					<label class="col-sm-4 col-form-label">生年月日</label>
					<div class="col-sm-8">
						<input type="date" class="form-control" name="birthDate" value="${strBirthDate}" required>
					</div>
				</div>
				<div class="form-group col-sm-12 mx-auto row">
					<label class="col-sm-4 col-form-label">電話番号</label>
					<div class="col-sm-8">
						<input type="tel" class="form-control" name="phoneNumber" value="${strPhoneNumber}" required>
					</div>
				</div>
				<div class="form-group col-sm-12 mx-auto row">
					<label class="col-sm-4 col-form-label">メールアドレス</label>
					<div class="col-sm-8">
						<input type="email" class="form-control" name="email" value="${strEmail}" required>
					</div>
				</div>
			</div>
		</div>

		<br>

		<div class="card col-sm-8 mx-auto">
			<div class="card-header">ご住所</div>
			<div class="card-body">
				<div class="form-group col-sm-12 mx-auto row">
					<label class="col-sm-4 col-form-label">郵便番号</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" name="postalCode" value="${strPostalCode}" required>
					</div>
				</div>
				<div class="form-group col-sm-12 mx-auto row">
					<label class="col-sm-4 col-form-label">都道府県</label>
					<div class="col-sm-8">
						<select name="prefectures" class="form-control">
							<option value="北海道" <c:if test="${strPrefectures == '北海道'}">selected</c:if>>北海道</option>
							<option value="青森県" <c:if test="${strPrefectures == '青森県'}">selected</c:if>>青森県</option>
							<option value="岩手県" <c:if test="${strPrefectures == '岩手県'}">selected</c:if>>岩手県</option>
							<option value="宮城県" <c:if test="${strPrefectures == '宮城県'}">selected</c:if>>宮城県</option>
							<option value="秋田県" <c:if test="${strPrefectures == '秋田県'}">selected</c:if>>秋田県</option>
							<option value="山形県" <c:if test="${strPrefectures == '山形県'}">selected</c:if>>山形県</option>
							<option value="福島県" <c:if test="${strPrefectures == '福島県'}">selected</c:if>>福島県</option>
							<option value="茨城県" <c:if test="${strPrefectures == '茨城県'}">selected</c:if>>茨城県</option>
							<option value="栃木県" <c:if test="${strPrefectures == '栃木県'}">selected</c:if>>栃木県</option>
							<option value="群馬県" <c:if test="${strPrefectures == '群馬県'}">selected</c:if>>群馬県</option>
							<option value="埼玉県" <c:if test="${strPrefectures == '埼玉県'}">selected</c:if>>埼玉県</option>
							<option value="千葉県" <c:if test="${strPrefectures == '千葉県'}">selected</c:if>>千葉県</option>
							<option value="東京都" <c:if test="${strPrefectures == '東京都'}">selected</c:if>>東京都</option>
							<option value="神奈川県" <c:if test="${strPrefectures == '神奈川県'}">selected</c:if>>神奈川県</option>
							<option value="新潟県" <c:if test="${strPrefectures == '新潟県'}">selected</c:if>>新潟県</option>
							<option value="富山県" <c:if test="${strPrefectures == '富山県'}">selected</c:if>>富山県</option>
							<option value="石川県" <c:if test="${strPrefectures == '石川県'}">selected</c:if>>石川県</option>
							<option value="福井県" <c:if test="${strPrefectures == '福井県'}">selected</c:if>>福井県</option>
							<option value="山梨県" <c:if test="${strPrefectures == '山梨県'}">selected</c:if>>山梨県</option>
							<option value="長野県" <c:if test="${strPrefectures == '長野県'}">selected</c:if>>長野県</option>
							<option value="岐阜県" <c:if test="${strPrefectures == '岐阜県'}">selected</c:if>>岐阜県</option>
							<option value="静岡県" <c:if test="${strPrefectures == '静岡県'}">selected</c:if>>静岡県</option>
							<option value="愛知県" <c:if test="${strPrefectures == '愛知県'}">selected</c:if>>愛知県</option>
							<option value="三重県" <c:if test="${strPrefectures == '三重県'}">selected</c:if>>三重県</option>
							<option value="滋賀県" <c:if test="${strPrefectures == '滋賀県'}">selected</c:if>>滋賀県</option>
							<option value="京都府" <c:if test="${strPrefectures == '京都府'}">selected</c:if>>京都府</option>
							<option value="大阪府" <c:if test="${strPrefectures == '大阪府'}">selected</c:if>>大阪府</option>
							<option value="兵庫県" <c:if test="${strPrefectures == '兵庫県'}">selected</c:if>>兵庫県</option>
							<option value="奈良県" <c:if test="${strPrefectures == '奈良県'}">selected</c:if>>奈良県</option>
							<option value="和歌山県" <c:if test="${strPrefectures == '和歌山県'}">selected</c:if>>和歌山県</option>
							<option value="鳥取県" <c:if test="${strPrefectures == '鳥取県'}">selected</c:if>>鳥取県</option>
							<option value="島根県" <c:if test="${strPrefectures == '島根県'}">selected</c:if>>島根県</option>
							<option value="岡山県" <c:if test="${strPrefectures == '岡山県'}">selected</c:if>>岡山県</option>
							<option value="広島県" <c:if test="${strPrefectures == '広島県'}">selected</c:if>>広島県</option>
							<option value="山口県" <c:if test="${strPrefectures == '山口県'}">selected</c:if>>山口県</option>
							<option value="徳島県" <c:if test="${strPrefectures == '徳島県'}">selected</c:if>>徳島県</option>
							<option value="香川県" <c:if test="${strPrefectures == '香川県'}">selected</c:if>>香川県</option>
							<option value="愛媛県" <c:if test="${strPrefectures == '愛媛県'}">selected</c:if>>愛媛県</option>
							<option value="高知県" <c:if test="${strPrefectures == '高知県'}">selected</c:if>>高知県</option>
							<option value="福岡県" <c:if test="${strPrefectures == '福岡県'}">selected</c:if>>福岡県</option>
							<option value="佐賀県" <c:if test="${strPrefectures == '佐賀県'}">selected</c:if>>佐賀県</option>
							<option value="長崎県" <c:if test="${strPrefectures == '長崎県'}">selected</c:if>>長崎県</option>
							<option value="熊本県" <c:if test="${strPrefectures == '熊本県'}">selected</c:if>>熊本県</option>
							<option value="大分県" <c:if test="${strPrefectures == '大分県'}">selected</c:if>>大分県</option>
							<option value="宮崎県" <c:if test="${strPrefectures == '宮崎県'}">selected</c:if>>宮崎県</option>
							<option value="鹿児島県" <c:if test="${strPrefectures == '鹿児島県'}">selected</c:if>>鹿児島県</option>
							<option value="沖縄県" <c:if test="${strPrefectures == '沖縄県'}">selected</c:if>>沖縄県</option>
						</select>
					</div>
				</div>
				<div class="form-group col-sm-12 mx-auto row">
					<label class="col-sm-4 col-form-label">市町村</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" name="address1" value="${strAddress1}" required>
					</div>
				</div>
				<div class="form-group col-sm-12 mx-auto row">
					<label class="col-sm-4 col-form-label">住所1（番地）</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" name="address2" value="${strAddress2}" required>
					</div>
				</div>
				<div class="form-group col-sm-12 mx-auto row">
					<label class="col-sm-4 col-form-label">住所2（建物名）</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" name="address3" value="${strAddress3}">
					</div>
				</div>
			</div>
		</div>

		<br>

		<div class="card col-sm-8 mx-auto">
			<div class="card-header">パスワード</div>
			<div class="card-body">
				<div class="form-group col-sm-12 mx-auto row">
					<label class="col-sm-4 col-form-label">パスワード</label>
					<div class="col-sm-8">
						<input type="password" class="form-control" name="password" required>
					</div>
				</div>
				<div class="form-group col-sm-12 mx-auto row">
					<label class="col-sm-4 col-form-label">パスワード（再入力）</label>
					<div class="col-sm-8">
						<input type="password" class="form-control" name="password_again" required>
					</div>
				</div>
			</div>
		</div>

		<br>

		<div align="center">
			<button type="submit" class="btn btn-primary">次に進む</button>
		</div>
	</form>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>
