<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>itemmaster</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />

	<div class="col-sm-9 mx-auto mt-sm-4">
		<h3>商品マスタ/一覧</h3>
		<hr>

		<c:if test="${msg != null}">
			<div class="alert alert-success mt-sm-5" role="alert">${msg}</div>
		</c:if>

		<div class="m-md-3">
			<div class="card bg-light p-lg-3 mx-auto">
				<form class="form-inlinecol col-sm-11 mx-auto" action="ItemMaster" method="post">
					<div class="form-row ml-sm-2">
						<div class="form-group col-sm-4 mr-sm-4">
							<label for="inputPassword2" class="sr-only">商品id</label> <input
								type="text" class="form-control" name="id"
								placeholder="商品id">
						</div>
						<div class="form-group col-sm-4">
							<label for="inputPassword2" class="sr-only">商品名</label> <input
								type="text" class="form-control" name="name"
								placeholder="商品名">
						</div>
					</div>

					<div class="form-row ml-sm-2">
						<div class="form-group col-sm-4 mr-sm-4">
							<select class="form-control" name="category">
								<option value="" selected>カテゴリ</option>
								<option>お菓子</option>
								<option>海産物</option>
								<option>農産物</option>
								<option>盛岡冷麺・麺類</option>
								<option>お酒・飲料</option>
								<option>お弁当・パン</option>
								<option>調味料・お漬物</option>
								<option>工芸品</option>
							</select>
						</div>
						<div class="form-group col-sm-2">
							<select class="form-control" name="stock">
								<option value="" selected>在庫</option>
								<option value=">= 1">在庫あり</option>
								<option value="BETWEEN 1 AND 10">在庫少</option>
								<option value="= 0" >在庫なし</option>
							</select>
						</div>
						<div class="form-group col-sm-3">
							<select class="form-control" name="statusId">
								<option value="" selected>商品状態</option>
								<option value="1">常温商品</option>
								<option value="2">クール便商品</option>
							</select>
						</div>
					</div>

					<div class="row ml-sm-auto">
						<div class="col-sm-4">
							<input type="date" name="startDate"
								class="form-control form-control-sm" id="colFormLabelSm">
						</div>
						〜
						<div class="col-sm-4">
							<input type="date" name="endDate"
								class="form-control form-control-sm" id="colFormLabelSm">
						</div>
					</div>

					<div align="right" class="mt-sm-3">
						<a href="ItemMasterRegist" type="button" class="btn btn-outline-danger">新規登録</a>
					</div>

					<div align="center" class="col-sm-4 mx-sm-auto">
						<button type="submit" class="btn btn-outline-success mb-4">この条件で検索する</button>
					</div>
				</form>
			</div>
		</div>

		<div class="col-sm-11 mx-auto">
			<p class="lead text-right pr-md-5">${itemCount}件</p>
		</div>

		<table class="table mb-sm-5 my-sm-4" style="font-size: 9pt">
			<thead style="font-size: 10pt">
				<tr class="table-secondary">
					<th class="text-center" style="width: 10%">商品ID</th>
					<th class="text-center" style="width: 10%">商品画像</th>
					<th class="text-center" style="width: 15%">価格（円）</th>
					<th class="text-center" style="width: 28%">商品名</th>
					<th class="text-center" style="width: 10%">在庫</th>
					<th class="text-center" style="width: 8%">詳細</th>
					<th class="text-center" style="width: 8%">更新</th>
					<th class="text-center" style="width: 8%">削除</th>
				</tr>
			</thead>
			<tbody>
			<c:forEach var="ItemList" items="${itemList}">
				<tr>
					<td class="align-middle" style="">${ItemList.id }</td>
					<td class="align-middle" style="width: 180px"><img class="img"
						src="images/${ItemList.fileName}" style="width: 150px"></td>
					<td class="align-middle" style="">¥${ItemList.price}</td>
					<td class="align-middle" style="">${ItemList.name}</td>
					<td class="align-middle" style="">${ItemList.stock}</td>
					<td class="align-middle" style="">
						<a href="ItemMasterDetail?id=${ItemList.id}" type="submit" class="btn btn-secondary btn-sm">詳細</a>
					</td>
					<td class="align-middle" style="">
						<a href="ItemMasterUpdate?id=${ItemList.id}" type="submit" class="btn btn-secondary btn-sm">更新</a>
					</td>
					<td class="align-middle" style="">
						<a href="ItemMasterDelete?id=${ItemList.id}" type="submit" class="btn btn-secondary btn-sm">削除</a>
					</td>
				</tr>
				</c:forEach>
			</tbody>
		</table>

		<a href="Index"><button type="button" class="col-sm-3 btn btn-dark">トップページへ戻る</button></a>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>