<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>creditcardupdate</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />

	<div class="col-sm-9 mx-auto mt-sm-4">
		<h3>マイページ/クレジットカード更新</h3>
		<hr>

		<div class="btn-group col-sm-12" role="group">
			<a href="User" type="button" class="btn btn-outline-dark col-sm-4">会員情報</a>
			<a href="UserUpdate" type="button" class="btn btn-outline-dark col-sm-4">会員情報更新</a>
			<a href="UserBuyHistory" type="button" class="btn btn-outline-dark col-sm-4">注文履歴</a>
			<a href="UserDelete" type="button" class="btn btn-outline-dark col-sm-4">退会手続き</a>
		</div>

		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>

		<form action="CreditCardUpdate" method="post">
			<table class="table mb-sm-5 my-sm-5" style="font-size: 9pt">
				<tbody>
					<tr>
						<th class="table-secondary" style="width: 60px">カード会社</th>
						<td class="" style="width: 180px">
							<div class="col-sm-10 row">
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" value="Visa"
										name="company" id="inlineRadio1" required>
									<label class="form-check-label" for="inlineRadio1"
										style="font-size: 12px">Visa</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" value="MasterCard"
										name="company" id="inlineRadio2" required>
									<label class="form-check-label" for="inlineRadio2"
										style="font-size: 12px">MasterCard</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" value="JCB"
										name="company" id="inlineRadio3" required>
									<label class="form-check-label" for="inlineRadio3"
										style="font-size: 12px">JCB</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" value="American Express"
										name="company" id="inlineRadio4" required>
									<label class="form-check-label" for="inlineRadio4"
										style="font-size: 12px">American Express</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" value="DinersClub"
										name="company" id="inlineRadio5" required>
									<label class="form-check-label" for="inlineRadio5"
										style="font-size: 12px">DinersClub</label>
								</div>
							</div>
					</tr>
					<tr>
						<th class="table-secondary" style="width: 60px">カード番号</th>
						<td class="" style="width: 180px">
							<div class="col-sm-4 row">
								<input type="text" class="form-control form-control-sm"
									name="number" value="${creditcard.number}" required>
							</div>
							<div style="font-size: 12px">（セキュリティ保護のため、登録済みのカード番号は一部伏字で表示されます）</div>
					</tr>
					<tr>
						<th class="table-secondary" style="width: 60px">カード名義人</th>
						<td class="" style="width: 180px">
							<div class="col-sm-4 row">
								<input type="text" class="form-control form-control-sm"
									name="name" value="${creditcard.name}" required>
							</div>
					</tr>
					<tr>
						<th class="table-secondary" style="width: 60px">有効期限</th>
						<td class="" style="width: 180px">
							<div class="col-sm-6 row">
								<input type="text" class="form-control form-control-sm col-sm-2"
									name="month" value="${creditcard.month}" required>
								<p style="font-size: 18px">月/</p>
								<input type="text" class="form-control form-control-sm col-sm-2"
									name="year" value="${creditcard.year}" required>
								<p style="font-size: 18px">年</p>
								<input type="hidden" class="form-control form-control-sm col-sm-2"
									name="id" value="${creditcard.id}">
							</div>
					</tr>

				</tbody>
			</table>
			<button class="btn btn-lg btn-primary btn-block col-sm-3 mx-auto"
				type="submit">更新</button>
		</form>


		<a href="User"><button type="button" class="col-sm-3 btn btn-dark">戻る</button></a>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>