<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>itemmasterregist</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />

	<div class="col-sm-9 mx-auto mt-sm-4">
		<h3>商品マスタ/商品詳細</h3>
		<hr>

		<table class="table mb-sm-5" style="font-size: 9pt">
			<tbody>
				<tr>
					<th class="table-secondary" style="width: 60px">商品ID</th>
					<td class="" style="width: 180px">${item.id}</td>
				</tr>
				<tr>
					<th class="table-secondary" style="width: 60px">商品名</th>
					<td class="" style="width: 180px">${item.name}</td>
				</tr>
				<tr>
					<th class="table-secondary" style="width: 60px">カテゴリ</th>
					<td class="" style="width: 180px">${item.category}</td>
				</tr>
				<tr>
					<th class="table-secondary" style="width: 60px">価格</th>
					<td class="" style="width: 180px">¥${item.price}</td>
				</tr>
				<tr>
					<th class="table-secondary" style="width: 60px">商品状態</th>
					<td class="" style="width: 180px">${item.statusName}</td>
				</tr>
				<tr>
					<th class="table-secondary" style="width: 60px">在庫</th>
					<td class="" style="width: 180px">${item.stock}</td>
				</tr>
				<tr>
					<th class="table-secondary" style="width: 60px">画像データ</th>
					<td class="" style="width: 180px"><img class=""
						src="images/${item.fileName}" alt="" width="100" height="">
						${item.fileName}</td>
				</tr>
				<tr>
					<th class="table-secondary" style="width: 60px">商品詳細</th>
					<td class="" style="width: 180px">${item.detail}</td>
				</tr>
				<tr>
					<th class="table-secondary" style="width: 60px">製造者</th>
					<td class="" style="width: 180px">${item.manufacture}</td>
				</tr>
				<tr>
					<th class="table-secondary" style="width: 60px">内容量</th>
					<td class="" style="width: 180px">${item.amount}</td>
				</tr>
				<tr>
					<th class="table-secondary" style="width: 60px">登録日</th>
					<td class="" style="width: 180px">${item.createDate}</td>
				</tr>
				<tr>
					<th class="table-secondary" style="width: 60px">更新日</th>
					<td class="" style="width: 180px">${item.updateDate}</td>
				</tr>
			</tbody>
		</table>

		<a href="ItemMaster" type="buttan" class="btn btn-outline-dark col-sm-1 mb-4">戻る</a>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>