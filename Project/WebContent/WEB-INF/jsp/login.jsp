<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>login</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<style>body,html {height: 100%;}</style>

</head>
	<body class="text-center">
		<jsp:include page="/baselayout/header.jsp" />

		<div class="container d-flex h-100">
			<div class="row align-self-center w-100">
				<div class="col-6 mx-auto">
					<div class="jumbotron">
						<form class="form-signin" action="Login" method="post">
							<img class="mb-4" src="images/logo.png" alt="" width="72" height="72">
							<h1 class="h3 mb-3 font-weight-normal">サインイン</h1>
							<label for="inputEmail" class="sr-only">Email address</label>
								<input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
							<label for="inputPassword" class="sr-only">Password</label>
								<input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
							<br>
							<c:if test="${errMsg != null}" >
								<div class="alert alert-danger" role="alert">
									${errMsg}
								</div>
							</c:if>
							<button class="btn btn-lg btn-primary btn-block" type="submit">サインイン</button>
							<div align="right" class="mt-sm-3">
								<a href="Regist"><button type="button" class="btn btn-outline-danger">新規登録</button></a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<jsp:include page="/baselayout/footer_bottom.jsp" />
	</body>
</html>
