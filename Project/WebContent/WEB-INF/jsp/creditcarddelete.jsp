<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>creditcardderete</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />

	<div class="col-sm-9 mx-auto mt-sm-4">
		<h3>マイページ/クレジットカード削除</h3>
		<hr>

		<div class="btn-group col-sm-12" role="group">
			<a href="User" type="button" class="btn btn-outline-dark col-sm-4">会員情報</a>
			<a href="UserUpdate" type="button" class="btn btn-outline-dark col-sm-4">会員情報更新</a>
			<a href="UserBuyHistory" type="button" class="btn btn-outline-dark col-sm-4">注文履歴</a>
			<a href="UserDelete" type="button" class="btn btn-outline-dark col-sm-4">退会手続き</a>
		</div>

		<table class="table mb-sm-5 mt-sm-5" style="font-size: 13pt">
			<p class="mt-sm-3">こちらのクレジットカードを削除してもよろしいですか？</p>
			<tbody>
				<tr>
					<th class="table-secondary" style="width: 60px">${creditcard.company}</th>
					<td class="" style="width: 120px">${creditcard.number}</td>
				</tr>
			</tbody>
		</table>


		<div class="row col-6 mx-auto">
			<div class="col-sm-6">
				<a href="User"><button type="button" class="btn btn-secondary btn-block""col-sm-3">キャンセル</button></a>
			</div>

			<div class="col-sm-6">
				<form action="CreditCardDelete" method="post">
					<input type="hidden" name="id" class="form-control form-control-sm"
						id="colFormLabelSm" value="${creditcard.id}" readonly>
					<button type="submit" class="btn btn-dark btn-block""col-sm-3">削除</button>
				</form>
			</div>
		</div>

	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>

</html>