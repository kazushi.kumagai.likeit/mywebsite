<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>buyuserregist</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
</head>
<body>

	<jsp:include page="/baselayout/header.jsp" />

	<div class="col-sm-9 mx-auto my-sm-5">
		このサービスをご利用になるにはログインしてください
		<div class="mx-auto">
			<div class="my-sm-3 row">
				<table class="table col-sm-5 mr-sm-5" style="height: 100px">
					<thead style="font-size: 10pt">
						<tr class="table-secondary">
							<th style="width: 400px">ログイン</th>
						</tr>
					</thead>
					<tbody style="font-size: 14px">

						<tr>

							<td scope="row" class="">
								<div class="mr-md-3 pt-3 px-3 py-md-3" style="font-size: 12px">
									<form action="Login" method="post">
										<label for="inputEmail" class="sr-only">Email address</label>
										<input type="email" name="email" class="form-control"
											placeholder="Email address" required autofocus> <label
											for="inputPassword" class="sr-only">Password</label> <input
											type="password" name="password" class="form-control"
											placeholder="Password" required> <br>
										<button class="btn btn-lg btn-primary btn-block" type="submit">ログイン</button>
									</form>
								</div>
							</td>
						</tr>
					</tbody>
				</table>

				<table class="table col-sm-5" style="height: 100px">
					<thead style="font-size: 10pt">
						<tr class="table-secondary">
							<th style="width: 400px">まだ会員登録されていない方</th>
						</tr>
					</thead>
					<tbody style="font-size: 14px">

						<tr>
							<td scope="row" class="">
								<div class="mr-md-3 pt-3 px-3 py-md-3" style="font-size: 12px">
									<a href="Regist"><button type="button" class="btn btn-outline-danger">新規登録</button></a>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />

</body>
</html>