<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>userupdate</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />

	<div class="col-sm-9 mx-auto mt-sm-4">
		<h3>マイページ/パスワード更新</h3>
		<hr>

		<div class="btn-group col-sm-12" role="group">
			<a href="User" type="button" class="btn btn-outline-dark col-sm-4">会員情報</a>
			<a href="UserUpdate" type="button"
				class="btn btn-outline-dark col-sm-4">会員情報更新</a> <a
				href="UserBuyHistory" type="button"
				class="btn btn-outline-dark col-sm-4">注文履歴</a> <a href="UserDelete"
				type="button" class="btn btn-outline-dark col-sm-4">退会手続き</a>
		</div>

		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>

		<form form action="UserPasswordUpdate" method="post">
			<table class="table mb-sm-5 my-sm-5" style="font-size: 9pt">
				<tbody>
					<tr>
						<th class="table-secondary" style="width: 60px">現在のパスワード</th>
						<td class="" style="width: 180px"><label for="inputEmail"
							class="sr-only">パスワード</label> <input type="password"
							name="password_now" class="form-control" required autofocus></td>
					</tr>
				</tbody>
			</table>

			<table class="table mb-sm-5 my-sm-5" style="font-size: 9pt">
				<tbody>
					<tr>
						<th class="table-secondary" style="width: 60px">新しいパスワード</th>
						<td class="" style="width: 180px"><label for="inputEmail"
							class="sr-only">パスワード</label> <input type="password"
							class="form-control" name="password_new" required autofocus></td>
					</tr>
					<tr>
						<th class="table-secondary" style="width: 60px">新しいパスワード（確認用）</th>
						<td class="" style="width: 180px"><label for="inputEmail"
							class="sr-only">パスワード確認用</label> <input type="password"
							class="form-control" name="password_again" required autofocus></td>
						<input type="hidden" class="form-control form-control-sm col-sm-2"
									name="id" value="${user.id}">
						<input type="hidden" class="form-control form-control-sm col-sm-2"
									name="getPassword" value="${user.password}">
					</tr>
				</tbody>
			</table>
			<button class="btn btn-lg btn-primary btn-block col-sm-3 mx-auto"
				type="submit">更新</button>
		</form>


		<a href="User"><button type="button" class="col-sm-3 btn btn-dark">戻る</button></a>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>