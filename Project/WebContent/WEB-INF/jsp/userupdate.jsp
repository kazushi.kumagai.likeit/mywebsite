<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>userupdate</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />

	<div class="col-sm-9 mx-auto mt-sm-4">
		<h3>マイページ/会員情報更新</h3>
		<hr>

		<div class="btn-group col-sm-12" role="group">
			<a href="User" type="button" class="btn btn-outline-dark col-sm-4">会員情報</a>
			<a href="UserUpdate" type="button"
				class="btn btn-outline-dark col-sm-4">会員情報更新</a> <a
				href="UserBuyHistory" type="button"
				class="btn btn-outline-dark col-sm-4">注文履歴</a> <a href="UserDelete"
				type="button" class="btn btn-outline-dark col-sm-4">退会手続き</a>
		</div>

		<form action="UserUpdate" method="post">
			<table class="table mb-sm-5 my-sm-5" style="font-size: 9pt">
				<tbody>
					<tr>
						<th class="table-secondary" style="width: 60px">氏名</th>
						<td class="" style="width: 180px"><label
							class="sr-only">氏名</label> <input type="text" name="name"
							class="form-control" value="${user.name}" required autofocus></td>
					</tr>
					<tr>
						<th class="table-secondary" style="width: 60px">生年月日</th>
						<td class="" style="width: 180px"><label
							class="sr-only">生年月日</label> <input type="Date" name="birthDate"
							class="form-control" value="${user.birthDate}" required autofocus></td>
					</tr>
					<tr>
						<th class="table-secondary" style="width: 60px">電話番号</th>
						<td class="" style="width: 180px"><label
							class="sr-only">電話番号</label> <input type="text" name="phoneNumber"
							class="form-control" value="${user.phoneNumber}" required autofocus></td>
					</tr>
					<tr>
						<th class="table-secondary" style="width: 60px">メールアドレス</th>
						<td class="" style="width: 180px"><label
							class="sr-only">メールアドレス</label> <input type="email" name="email"
							class="form-control" value="${user.email}"
							required autofocus></td>
					</tr>
					<tr>
						<th class="table-secondary" style="width: 60px">郵便番号</th>
						<td class="" style="width: 180px"><label
							class="sr-only">郵便番号</label> <input type="text" name="postalCode"
							class="form-control" value="${user.postalCode}" required autofocus></td>
					</tr>
					<tr>
						<th class="table-secondary" style="width: 60px">都道府県</th>
						<td class="" style="width: 180px"><label
							class="sr-only">都道府県</label>
						<select name="prefectures" class="form-control">
							<option value="北海道" <c:if test="${user.prefectures == '北海道'}">selected</c:if>>北海道</option>
							<option value="青森県" <c:if test="${user.prefectures == '青森県'}">selected</c:if>>青森県</option>
							<option value="岩手県" <c:if test="${user.prefectures == '岩手県'}">selected</c:if>>岩手県</option>
							<option value="宮城県" <c:if test="${user.prefectures == '宮城県'}">selected</c:if>>宮城県</option>
							<option value="秋田県" <c:if test="${user.prefectures == '秋田県'}">selected</c:if>>秋田県</option>
							<option value="山形県" <c:if test="${user.prefectures == '山形県'}">selected</c:if>>山形県</option>
							<option value="福島県" <c:if test="${user.prefectures == '福島県'}">selected</c:if>>福島県</option>
							<option value="茨城県" <c:if test="${user.prefectures == '茨城県'}">selected</c:if>>茨城県</option>
							<option value="栃木県" <c:if test="${user.prefectures == '栃木県'}">selected</c:if>>栃木県</option>
							<option value="群馬県" <c:if test="${user.prefectures == '群馬県'}">selected</c:if>>群馬県</option>
							<option value="埼玉県" <c:if test="${user.prefectures == '埼玉県'}">selected</c:if>>埼玉県</option>
							<option value="千葉県" <c:if test="${user.prefectures == '千葉県'}">selected</c:if>>千葉県</option>
							<option value="東京都" <c:if test="${user.prefectures == '東京都'}">selected</c:if>>東京都</option>
							<option value="神奈川県" <c:if test="${user.prefectures == '神奈川県'}">selected</c:if>>神奈川県</option>
							<option value="新潟県" <c:if test="${user.prefectures == '新潟県'}">selected</c:if>>新潟県</option>
							<option value="富山県" <c:if test="${user.prefectures == '富山県'}">selected</c:if>>富山県</option>
							<option value="石川県" <c:if test="${user.prefectures == '石川県'}">selected</c:if>>石川県</option>
							<option value="福井県" <c:if test="${user.prefectures == '福井県'}">selected</c:if>>福井県</option>
							<option value="山梨県" <c:if test="${user.prefectures == '山梨県'}">selected</c:if>>山梨県</option>
							<option value="長野県" <c:if test="${user.prefectures == '長野県'}">selected</c:if>>長野県</option>
							<option value="岐阜県" <c:if test="${user.prefectures == '岐阜県'}">selected</c:if>>岐阜県</option>
							<option value="静岡県" <c:if test="${user.prefectures == '静岡県'}">selected</c:if>>静岡県</option>
							<option value="愛知県" <c:if test="${user.prefectures == '愛知県'}">selected</c:if>>愛知県</option>
							<option value="三重県" <c:if test="${user.prefectures == '三重県'}">selected</c:if>>三重県</option>
							<option value="滋賀県" <c:if test="${user.prefectures == '滋賀県'}">selected</c:if>>滋賀県</option>
							<option value="京都府" <c:if test="${user.prefectures == '京都府'}">selected</c:if>>京都府</option>
							<option value="大阪府" <c:if test="${user.prefectures == '大阪府'}">selected</c:if>>大阪府</option>
							<option value="兵庫県" <c:if test="${user.prefectures == '兵庫県'}">selected</c:if>>兵庫県</option>
							<option value="奈良県" <c:if test="${user.prefectures == '奈良県'}">selected</c:if>>奈良県</option>
							<option value="和歌山県" <c:if test="${user.prefectures == '和歌山県'}">selected</c:if>>和歌山県</option>
							<option value="鳥取県" <c:if test="${user.prefectures == '鳥取県'}">selected</c:if>>鳥取県</option>
							<option value="島根県" <c:if test="${user.prefectures == '島根県'}">selected</c:if>>島根県</option>
							<option value="岡山県" <c:if test="${user.prefectures == '岡山県'}">selected</c:if>>岡山県</option>
							<option value="広島県" <c:if test="${user.prefectures == '広島県'}">selected</c:if>>広島県</option>
							<option value="山口県" <c:if test="${user.prefectures == '山口県'}">selected</c:if>>山口県</option>
							<option value="徳島県" <c:if test="${user.prefectures == '徳島県'}">selected</c:if>>徳島県</option>
							<option value="香川県" <c:if test="${user.prefectures == '香川県'}">selected</c:if>>香川県</option>
							<option value="愛媛県" <c:if test="${user.prefectures == '愛媛県'}">selected</c:if>>愛媛県</option>
							<option value="高知県" <c:if test="${user.prefectures == '高知県'}">selected</c:if>>高知県</option>
							<option value="福岡県" <c:if test="${user.prefectures == '福岡県'}">selected</c:if>>福岡県</option>
							<option value="佐賀県" <c:if test="${user.prefectures == '佐賀県'}">selected</c:if>>佐賀県</option>
							<option value="長崎県" <c:if test="${user.prefectures == '長崎県'}">selected</c:if>>長崎県</option>
							<option value="熊本県" <c:if test="${user.prefectures == '熊本県'}">selected</c:if>>熊本県</option>
							<option value="大分県" <c:if test="${user.prefectures == '大分県'}">selected</c:if>>大分県</option>
							<option value="宮崎県" <c:if test="${user.prefectures == '宮崎県'}">selected</c:if>>宮崎県</option>
							<option value="鹿児島県" <c:if test="${user.prefectures == '鹿児島県'}">selected</c:if>>鹿児島県</option>
							<option value="沖縄県" <c:if test="${user.prefectures == '沖縄県'}">selected</c:if>>沖縄県</option>
						</select>
						</td>
					</tr>
					<tr>
						<th class="table-secondary" style="width: 60px">住所（郡市区）</th>
						<td class="" style="width: 180px"><label
							class="sr-only">住所（郡市区）</label> <input type="text" name="address1"
							class="form-control" value="${user.address1}" required
							autofocus></td>
					</tr>
					<tr>
						<th class="table-secondary" style="width: 60px">住所２（それ以降）</th>
						<td class="" style="width: 180px"><label
							class="sr-only">住所２（それ以降）</label> <input type="text" name="address2"
							class="form-control" value="${user.address2}" required
							autofocus></td>
					</tr>
					<tr>
						<th class="table-secondary" style="width: 60px">住所３（マンション名等）</th>
						<td class="" style="width: 180px"><label
							class="sr-only">住所３（マンション名等）</label> <input type="text" name="address3"
							class="form-control" value="${user.address3}"
							autofocus></td>
					</tr>
					<!-- <tr>
						<th class="table-secondary" style="width: 60px">パスワード</th>
						<td class="" style="width: 180px"><label
							class="sr-only">パスワード</label> <input type="password"
							class="form-control" value="" required autofocus></td>
					</tr>
					<tr>
						<th class="table-secondary" style="width: 60px">パスワード確認用</th>
						<td class="" style="width: 180px"><label
							class="sr-only">パスワード確認用</label> <input type="password"
							class="form-control" value="" required autofocus></td>
					</tr> -->
				</tbody>
			</table>
			<button class="btn btn-lg btn-primary btn-block col-sm-3 mx-auto"
				type="submit">更新</button>
		</form>


		<a href="Index"><button type="button" class="col-sm-3 btn btn-dark">トップページへ戻る</button></a>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>