<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>userbuyhistorydetail</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />

	<div class="col-sm-9 mx-auto mt-sm-4">
		<h3>マイページ/注文履歴詳細</h3>
		<hr>

		<div class="btn-group col-sm-12" role="group">
			<a href="User" type="button" class="btn btn-outline-dark col-sm-4">会員情報</a>
			<a href="UserUpdate" type="button"
				class="btn btn-outline-dark col-sm-4">会員情報更新</a> <a
				href="UserBuyHistory" type="button"
				class="btn btn-outline-dark col-sm-4">注文履歴</a> <a href="UserDelete"
				type="button" class="btn btn-outline-dark col-sm-4">退会手続き</a>
		</div>

		<p class="mt-sm-5 mb-sm-4">注文日：${buyData.formatDate} ｜ 注文ID：${buyData.id}</p>
		<table class="table mr-sm-4" style="height: 100px">
			<thead style="font-size: 10pt">
				<tr class="table-secondary">
					<th class="" style="width: 150px">お届け先住所</th>
					<th class="" style="width: 100px">お支払い方法</th>
					<th class="" style="width: 100px">ご請求額</th>
				</tr>
			</thead>
			<tbody style="font-size: 12px">
				<tr>
					<td>${user.name}<br>${user.postalCode}<br> ${user.prefectures}${user.address1}<br>${user.address2}<br>${user.address3}
					</td>
					<td class="align-middle">${buyData.payMethodName}</td>
					<td class="">
						<div class="row justify-content-between">
							商品の小計（税込）：
							<div class="mr-sm-4">¥${buyData.totalPrice - (buyData.deliveryMethodPrice + buyData.payMethodPrice)}</div>
						</div>
						<div class="row justify-content-between">
							配送料・手数料：
							<div class="mr-sm-4">¥${buyData.deliveryMethodPrice + buyData.payMethodPrice}</div>
						</div>
						<div class="row justify-content-between mt-sm-3">
							<div class="font-weight-bold">ご請求額：</div>
							<div class="mr-sm-4">¥${buyData.totalPrice}</div>
						</div>

					</td>
				</tr>
			</tbody>
		</table>

		<table class="table mb-sm-5 my-sm-5" style="font-size: 9pt">
			<tbody>
			<c:forEach var="ItemDataList" items="${ItemDataList}">
				<tr>

					<td class="" style="width: 120px">
						<div class="row mb-sm-3">
							<img class="img" src="images/${ItemDataList.fileName}" style="width: 150px">
							<div class="row">
								<p class="mt-sm-4 ml-sm-5">
									${ItemDataList.name}<br> 通常価格(税込) ¥${ItemDataList.taxPrice}
								</p>
							</div>
						</div>
						</td>
						<td class="mt-sm-4 ml-sm-2 align-middle" style="width: 80px">
							数量：${ItemDataList.number}
						</td>
						<td class="mt-sm-4 ml-sm-2 align-middle" style="width: 80px">
							合計（税込）：¥${ItemDataList.taxPrice * ItemDataList.number}
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>




		<a href="UserBuyHistory" type="button" class="col-sm-3 btn btn-dark">注文履歴へ戻る</a>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>