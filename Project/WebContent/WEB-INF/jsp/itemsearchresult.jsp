<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>itemsearchresult</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
<link href="/MyWebSite/css/product.css" rel="stylesheet">
<link href="/MyWebSite/css/card.css" rel="stylesheet">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />

	<div class="m-md-3">
		<div class="card-boby bg-light p-lg-3 mx-auto row">
			<form class="form-inlinecol col-sm-8 mx-auto row" action="ItemSearchResult">
				<div class="form-group col-sm-3">
					<select class="form-control" name="category">
						<option selected value="">カテゴリ</option>
						<option>お菓子</option>
						<option>海産物</option>
						<option>農産物</option>
						<option>盛岡冷麺・麺類</option>
						<option>お酒・飲料</option>
						<option>お弁当・パン</option>
						<option>調味料・お漬物</option>
						<option>工芸品</option>
					</select>
				</div>

				<div class="form-group col-sm-4 mb-2">
					<label class="sr-only">キーワード</label>
					<input type="text"class="form-control" placeholder="キーワード検索" name="name">
				</div>
				<button type="submit" class="btn btn-outline-success mb-4 ml-sm-5">Search</button>
			</form>
		</div>
	</div>

	<div class="col-sm-11 mx-auto">
		<p class="lead">検索結果</p>
		<hr>
		<p class="lead text-right pr-md-5">${itemCount}件</p>
	</div>

<div class="section pt-md-5 px-md-5">
			<!--   商品情報   -->
			<div class="row">
				<c:forEach var="ItemList" items="${itemList}" varStatus="status">
				<div class="col s12 m3 mb-sm-5">
					<div class="card-body bg-light text-center">
						<div class="card-image">
							<a href="Item?id=${ItemList.id}&category=${ItemList.category}"><img src="images/${ItemList.fileName}"></a>
						</div>
						<div class="card-content">
							<span class="card-title">${ItemList.name}</span>
							<p>${ItemList.price}円
							</p>
						</div>
					</div>
				</div>
				<c:if test="${(status.index + 1) % 3 == 0}">
			</div>
			<div class="row">
				</c:if>
				</c:forEach>
			</div>
		</div>

	<br>

	<div class="rowr">
	<nav aria-label="Page navigation example">
			<ul class="pagination justify-content-center">
				<!-- １ページ戻るボタン  -->
				<c:choose>
					<c:when test="${pageNum == 1}">
						<li class="disabled"><a></a></li>
					</c:when>
					<c:otherwise>
						<li class="page-item"><a class="page-link" href="ItemSearchResult?search_word=${searchWord}&category=${category}&page_num=${pageNum - 1}">Previous</a></li>
					</c:otherwise>
				</c:choose>

				<!-- ページインデックス -->
				<c:forEach begin="${(pageNum - 5) > 0 ? pageNum - 5 : 1}" end="${(pageNum + 5) > pageMax ? pageMax : pageNum + 5}" step="1" varStatus="status">
					<li class="page-item"><a class="page-link" href="ItemSearchResult?searchWord=${searchWord}&category=${category}&page_num=${status.index}">${status.index}</a></li>
				</c:forEach>

				<!-- 1ページ送るボタン -->
				<c:choose>
				<c:when test="${pageNum == pageMax || pageMax == 0}">
					<li class="disabled"><a></a></li>
				</c:when>
				<c:otherwise>
					<li class="page-item"><a class="page-link" href="ItemSearchResult?search_word=${searchWord}&category=${category}&page_num=${pageNum + 1}">Next</a></li>
				</c:otherwise>
				</c:choose>
			</ul>
			</nav>
		</div>

	<br>

	<jsp:include page="/baselayout/footer.jsp" />

</body>
</html>