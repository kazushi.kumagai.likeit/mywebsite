<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>itemmasterderete</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />

	<div class="col-sm-9 mx-auto mt-sm-4">
		<h3>商品マスタ/商品削除</h3>
		<hr>

		<p>こちらの商品を削除してもよろしいですか？</p>

		<div class="d-md-flex flex-md-equal w-100 mt-md-3 mb-md-5 pl-md-5">
			<div class="bg-light mr-md-3 text-center overflow-hidden">
				<div class="card">
					<img class="card-img" src="images/${item.fileName}">
				</div>
			</div>
			<div class="overflow-hidden col-sm-5 pt-md-5 mr-md-5">
				<h4 class="font-weight-bold">${item.name}</h4>
				<hr>
				<div class="row justify-content-between mt-sm-3">
					<div class="font-weight-bold">商品Id：</div>
					<div class="mr-sm-5">${item.id}</div>
				</div>
				<div class="row justify-content-between mt-sm-3">
					<div class="font-weight-bold">価格（円）：</div>
					<div class="mr-sm-5">¥${item.price}</div>
				</div>
				<div class="row justify-content-between mt-sm-3">
					<div class="font-weight-bold">在庫：</div>
					<div class="mr-sm-5">${item.stock}</div>
				</div>
			</div>
		</div>
		<div class="row col-6 mx-auto">
			<div class="col-sm-6">
				<a href="ItemMaster"><button type="button" class="btn btn-secondary btn-block""col-sm-3">キャンセル</button></a>
			</div>
			<div class="col-sm-6">
				<form action="ItemMasterDelete" method="post">
					<input type="hidden" name="id" class="form-control form-control-sm"
						id="colFormLabelSm" value="${item.id}" readonly>
					<button type="submit" class="btn btn-dark btn-block""col-sm-3">削除</button>
				</form>
			</div>
		</div>
	</div>
	<jsp:include page="/baselayout/footer_bottom.jsp" />
</body>
</html>