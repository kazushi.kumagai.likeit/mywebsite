<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>title</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
<link href="/MyWebSite/css/product.css" rel="stylesheet">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />

	<div class="d-md-flex flex-md-equal w-100 mt-md-3 mb-md-5 pl-md-5">
		<div class="bg-light mr-md-3 text-center overflow-hidden">
			<div class="card">
				<img class="card-img" src="images/${item.fileName}">
			</div>
		</div>
		<div class="overflow-hidden col-sm-5 pt-md-5 mr-md-5">
			<h2 class="display-4">${item.name}</h2>
			<hr>
			<p class="lead">通常価格 ¥${item.price}</p>
			<form action="ItemAdd" method="post" class="form-inline my-md-3">
				<label class="my-1 mr-2">数量</label>
				<input type="tel" class="form-control my-1 mr-sm-2 col-sm-1"
					name="number" value="1">
				<input type="hidden" name="id" class="form-control form-control-sm"
					value="${item.id}" readonly>
				<c:if test="${item.stock != 0}">
					<button type="submit" class="btn btn-dark col-sm-5 my-1">カートに入れる</button>
				</c:if>
				<c:if test="${item.stock == 0}">
					<p class="col-sm-5 my-1 text-danger">在庫なし</p>
				</c:if>
			</form>
			<c:if test="${item.statusId == 2}">
				<div class="col-sm-8 mx-auto alert alert-primary" role="alert"">
					<p>送料について</p>
					<p>こちらの商品はクール便商品のため送料に¥500かかります</p>
				</div>
			</c:if>
		</div>
	</div>

	<hr>

	<div class="d-md-flex flex-md-equal w-100 mt-md-3 mb-md-5 pl-md-5">
		<div class="pt-md-5 pr-md-5 mr-md-3 overflow-hidden">
			<h1 class="">商品説明</h1>
			<p>${item.detail}</p>
		</div>
		<div class="overflow-hidden col-sm-4 pt-md-5 mr-md-5">
			<h1 class="">スペック</h1>
			<div class="row">
				<p class="font-weight-bold col-sm-4">製造者</p>
				<p>${item.manufacture}</p>
			</div>
			<div class="row">
				<p class="font-weight-bold col-sm-4">セット内容</p>
				<p>${item.amount}</p>
			</div>
		</div>
	</div>

	<hr>

	<div class=" text-center mt-md-5">
		<h1 class="">関連する商品</h1>
		<div class="d-md-flex flex-md-equal w-100 my-md-3 pl-md-3">
		<c:forEach var="RandItemList" items="${randItemList}">
			<div class="mr-md-3 pt-3 px-3 pt-md-5 text-center overflow-hidden">
				<a href="Item?id=${RandItemList.id}&category=${RandItemList.category}"><img class="" src="images/${RandItemList.fileName}" width="auto" height="150px"></a>
				<p class="mt-sm-3">${RandItemList.name}</p>
			</div>
			</c:forEach>
		</div>
	</div>

	<br>

	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>