<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>buyinput</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />

	<div class="col-sm-9 mx-auto">
		<h2 class="my-sm-4">ご注文主様</h2>

		<table class="table mb-sm-5" style="font-size: 9pt">
			<tbody>
				<tr>
					<th class="table-secondary" style="width: 60px">注文者氏名</th>
					<td class="" style="width: 180px">${user.name}</td>
				</tr>
				<tr>
					<th class="table-secondary" style="width: 60px">注文者電話番号</th>
					<td class="" style="width: 180px">${user.phoneNumber}</td>
				</tr>
				<tr>
					<th class="table-secondary" style="width: 60px">メールアドレス</th>
					<td class="" style="width: 180px">${user.email}</td>
				</tr>
				<tr>
					<th class="table-secondary" style="width: 60px">注文者郵便番号</th>
					<td class="" style="width: 180px">${user.postalCode}</td>
				</tr>
				<tr>
					<th class="table-secondary" style="width: 60px">注文者都道府県</th>
					<td class="" style="width: 180px">${user.prefectures}</td>
				</tr>
				<tr>
					<th class="table-secondary" style="width: 60px">注文者住所（郡市区）</th>
					<td class="" style="width: 180px">${user.address1}</td>
				</tr>
				<tr>
					<th class="table-secondary" style="width: 60px">注文者住所２（それ以降）</th>
					<td class="" style="width: 180px">${user.address2}</td>
				</tr>
				<tr>
					<th class="table-secondary" style="width: 60px">注文者郵便番号３（マンション名等）</th>
					<td class="" style="width: 180px">${user.address3}</td>
				</tr>
			</tbody>
		</table>

		<h2 class="mt-sm-5 mb-sm-4">お届けする商品</h2>
		<table class="table mr-sm-4" style="height: 100px">
			<thead style="font-size: 10pt">
				<tr class="table-secondary">
					<th class="text-center" style="width: 400px">商品名</th>
					<th class="text-center" style="width: 60px">数量</th>
					<th class="text-center" style="width: 150px">小計（税抜）</th>
				</tr>
			</thead>
			<tbody style="font-size: 12px">
			<c:forEach var="item" items="${cart}" varStatus="status">
				<tr>
					<td scope="row" class="align-middle">
						<div class="mr-md-3 pt-3 px-3 py-md-3 row">
							<img class="" src="images/${item.fileName}" alt="" width="150" height="">
							<p class="mt-sm-3 ml-sm-4 py-sm-4">${item.name}</p>
						</div>
					</td>
					<td class="align-middle">${item.number}</td>
					<td class="align-middle">￥${item.price * item.number}</td>
				</tr>
				</c:forEach>
			</tbody>
		</table>



		<h2 class="my-sm-4">配送方法</h2>
		<form action="BuyConfirm" method="post">
			<div class="card">
				<div class="card-header">配送希望日時指定</div>
				<div class="card-body">
					<div class="form-group col-sm-12 mx-auto row">
						<label for="inputPassword" class="col-sm-2 col-form-label"
							style="font-size: 12px">配送希望日：</label>
						<div class="row">
							<select name="deliveryDay" class="form-control form-control-sm"
								style="font-size: 12px">
								<option selected>希望なし</option>
								<option>${strNextDate}</option>
								<option>${dateLater2}</option>
								<option>${dateLater3}</option>
								<option>${dateLater4}</option>
								<option>${dateLater5}</option>
							</select>
						</div>
					</div>
					<div class="form-group col-sm-12 mx-auto row">
						<label for="inputPassword" class="col-sm-2 col-form-label"
							style="font-size: 12px">配送希望時間：</label>
						<div class="col-sm-8 row">
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" required checked="checked"
									name="deliveryDate" value="希望なし">
								<label class="form-check-label" for="inlineRadio1"
									style="font-size: 12px">希望なし</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" required
									name="deliveryDate" value="午前中">
								<label class="form-check-label" for="inlineRadio2"
									style="font-size: 12px">午前中</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" required
									name="deliveryDate" value="14~16時">
								<label class="form-check-label" for="inlineRadio2"
									style="font-size: 12px">14~16時</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" required
									name="deliveryDate" value="16~18時">
								<label class="form-check-label" for="inlineRadio2"
									style="font-size: 12px">16~18時</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" required
									name="deliveryDate" value="18~20時">
								<label class="form-check-label" for="inlineRadio2"
									style="font-size: 12px">18~20時</label>
							</div>
						</div>
					</div>
					<div class="ml-sm-4" style="font-size: 12px">
						*日時指定配送は送料が¥200発生します。
					</div>
				</div>
			</div>

			<h2 class="my-sm-4">お支払い情報</h2>
			<div class="card">
				<div class="card-header">支払い方法</div>
				<div class="card-body">
					<div class="form-group col-sm-12 mx-auto row">
						<div class="col-sm-8 row">
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio"
									name="payMethod" required value="代金引換">
								<label class="form-check-label"
									style="font-size: 12px">代金引換</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio"
									name="payMethod" required value="クレジットカード">
								<label class="form-check-label"
									style="font-size: 12px">クレジットカード</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio"
									name="payMethod" required value="コンビニ前払い">
								<label class="form-check-label"
									style="font-size: 12px">コンビニ前払い</label>
							</div>
						</div>
					</div>
					<div class="ml-sm-4" style="font-size: 12px">*代金引換は手数料が¥300発生します。</div>
				</div>
			</div>

			<div class="card col-sm-7 mx-auto my-sm-4">
				<div class="mt-sm-4 text-center">
					<h3>合計金額：¥${totalPrice.totalPrice}（税抜）</h3>
				</div>
				<div class="my-sm-3 text-center" style="font-size: 12px">※送料・手数料の金額は含まれていません。次のご確認画面でご確認いただけます。</div>

			</div>

			<div class="">
			<c:forEach var="item" items="${cart}" varStatus="status">
			<input type="hidden" readonly class="form-control-plaintext"
							 value="${item.statusId}" name="statusId" readonly>
			</c:forEach>
				<a href="Cart"><button type="button" class="btn btn-outline-secondary">戻る</button></a>
				<button class="btn btn-lg btn-dark btn-block col-sm-5 mx-auto"
					type="submit">設定して注文の確認画面に進む</button>
			</div>
		</form>
	</div>

	<br>

	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>