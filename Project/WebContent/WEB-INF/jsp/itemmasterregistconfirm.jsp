<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>itemmasterregistconfirm</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />

	<div class="col-sm-9 mx-auto mt-sm-4">
		<h3>商品マスタ/商品登録確認</h3>
		<hr>

		<div class="">以下の内容で登録します。よろしいですか？</div>
		<form action="ItemMasterRegistConfirm" method="post">
			<table class="table mb-sm-5" style="font-size: 9pt">
				<tbody>
					<tr>
						<th class="table-secondary" style="width: 60px">商品名</th>
						<td class="" style="width: 180px"><input type="text" readonly
							class="form-control-plaintext" value="${strName}" name="name"
							readonly></td>
					</tr>
					<tr>
						<th class="table-secondary" style="width: 60px">カテゴリ</th>
						<td class="" style="width: 180px"><input type="text" readonly
							class="form-control-plaintext" value="${strCategory}"
							name="category" readonly></td>
					</tr>
					<tr>
						<th class="table-secondary" style="width: 60px">価格</th>
						<td class="" style="width: 180px"><input type="tel" readonly
							class="form-control-plaintext" value="${strPrice}" name="price"
							readonly></td>
					</tr>
					<tr>
						<th class="table-secondary" style="width: 60px">商品状態</th>
						<td class="" style="width: 180px"><input type="text" readonly
							class="form-control-plaintext" value="${strStatusId}"
							name="statusId" readonly> * 1 = 常温商品 | 2 = クール便商品</td>
					</tr>
					<tr>
						<th class="table-secondary" style="width: 60px">在庫</th>
						<td class="" style="width: 180px"><input type="text" readonly
							class="form-control-plaintext" value="${strStock}" name="stock"
							readonly></td>
					</tr>
					<tr>
						<th class="table-secondary" style="width: 60px">画像データファイル名</th>
						<td class="" style="width: 180px"><input type="text" readonly
							class="form-control-plaintext" value="${strFileName}"
							name="fileName" readonly></td>
					</tr>
					<tr>
						<th class="table-secondary" style="width: 60px">商品詳細</th>
						<td class="" style="width: 180px"><textarea
								class="form-control" name="detail" rows="3" readonly>${strDetail}</textarea></td>
					</tr>
					<tr>
						<th class="table-secondary" style="width: 60px">製造者</th>
						<td class="" style="width: 180px"><input type="text" readonly
							class="form-control-plaintext" value="${strManufacture}"
							name="manufacture" readonly></td>
					</tr>
					<tr>
						<th class="table-secondary" style="width: 60px">内容量</th>
						<td class="" style="width: 180px"><input type="text" readonly
							class="form-control-plaintext" value="${strAmount}" name="amount"
							readonly></td>
					</tr>
				</tbody>
			</table>
			<div class="">内容を確認し、よろしければ登録ボタンを押してください。</div>

			<br>

			<div class="row col-6 mx-auto mb-sm-5">
				<div class="col-sm-6">
					<button type="submit" class="btn btn-secondary btn-block"
						name="confirm_button" value="cancel">入力画面に戻る</button>
				</div>
				<div class="col-sm-6">
					<button type="submit" class="btn btn-primary btn-block"
						name="confirm_button" value="regist">登録</button>
				</div>
			</div>
		</form>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>