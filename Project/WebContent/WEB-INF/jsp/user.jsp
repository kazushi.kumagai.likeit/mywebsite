<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>user</title>
<link rel="stylesheet"href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"crossorigin="anonymous">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />

	<div class="col-sm-9 mx-auto mt-sm-4">
		<h3>マイページ/会員情報</h3>
		<hr>

		<div class="btn-group col-sm-12" role="group">
			<a href="User" type="button" class="btn btn-outline-dark col-sm-4">会員情報</a>
			<a href="UserUpdate" type="button" class="btn btn-outline-dark col-sm-4">会員情報更新</a>
			<a href="UserBuyHistory" type="button" class="btn btn-outline-dark col-sm-4">注文履歴</a>
			<a href="UserDelete" type="button" class="btn btn-outline-dark col-sm-4">退会手続き</a>
		</div>

		<c:if test="${msg != null}">
			<div class="alert alert-success mt-sm-5" role="alert">${msg}</div>
		</c:if>

		<table class="table mb-sm-5 my-sm-5" style="font-size: 9pt">
			<tbody>
				<tr>
					<th class="table-secondary" style="width: 60px">氏名</th>
					<td class="" style="width: 180px">${user.name}</td>
				</tr>
				<tr>
					<th class="table-secondary" style="width: 60px">生年月日</th>
					<td class="" style="width: 180px">${user.birthDate}</td>
				</tr>
				<tr>
					<th class="table-secondary" style="width: 60px">電話番号</th>
					<td class="" style="width: 180px">${user.phoneNumber}</td>
				</tr>
				<tr>
					<th class="table-secondary" style="width: 60px">メールアドレス</th>
					<td class="" style="width: 180px">${user.email}</td>
				</tr>
				<tr>
					<th class="table-secondary" style="width: 60px">郵便番号</th>
					<td class="" style="width: 180px">${user.postalCode}</td>
				</tr>
				<tr>
					<th class="table-secondary" style="width: 60px">都道府県</th>
					<td class="" style="width: 180px">${user.prefectures}</td>
				</tr>
				<tr>
					<th class="table-secondary" style="width: 60px">住所（郡市区）</th>
					<td class="" style="width: 180px">${user.address1}</td>
				</tr>
				<tr>
					<th class="table-secondary" style="width: 60px">住所２（それ以降）</th>
					<td class="" style="width: 180px">${user.address2}</td>
				</tr>
				<tr>
					<th class="table-secondary" style="width: 60px">住所３（マンション名等）</th>
					<td class="" style="width: 180px">${user.address3}</td>
				</tr>
			</tbody>
		</table>

		<div align="right" class="mt-sm-3">
			<a href="UserPasswordUpdate"><button type="button"
					class="btn btn-outline-danger">パスワード更新</button></a>
		</div>

		<c:if test="${creditCardList != '[]'}">
			<p>お支払い情報</p>
			<table class="table mb-sm-5 mb-sm-5" style="font-size: 13pt">
				<tbody>
					<c:forEach var="creditcardList" items="${creditCardList}">
						<tr>
							<th class="table-secondary" style="width: 60px">${creditcardList.company}</th>
							<td class="" style="width: 120px">${creditcardList.number}</td>
							<td class="" style="width: 90px"><a
								href="CreditCardUpdate?id=${creditcardList.id}" type="button"
								class="col-sm-3 btn btn-dark">編集</a> <a
								href="CreditCardDelete?id=${creditcardList.id}" type="button"
								class="col-sm-3 btn btn-dark">削除</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:if>

		<a href="Index"><button type="button" class="col-sm-3 btn btn-dark">トップページへ戻る</button></a>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>