<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>registconfirm</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />

	<div class="col-sm-11 mx-auto">
		<h3>会員登録 確認画面</h3>
	</div>

	<br>

	<div class="col-sm-10 mx-auto">以下の内容で登録します。よろしいですか？</div>

	<form action="RegistConfirm" method="post">
		<div class="card col-sm-8 mx-auto">
			<div class="card-header">個人情報</div>
			<div class="card-body">
				<div class="form-group col-sm-12 mx-auto row">
					<label  class="col-sm-4 col-form-label">名前</label>
					<div class="col-sm-8">
						<input type="text" readonly class="form-control-plaintext"
							value="${strName}" name="name" readonly>
					</div>
				</div>
				<div class="form-group col-sm-12 mx-auto row">
					<label class="col-sm-4 col-form-label">生年月日</label>
					<div class="col-sm-8">
						<input type="date" readonly class="form-control-plaintext"
							value="${strBirthDate}" name="birthDate" readonly>
					</div>
				</div>
				<div class="form-group col-sm-12 mx-auto row">
					<label class="col-sm-4 col-form-label">電話番号</label>
					<div class="col-sm-8">
						<input type="text" readonly class="form-control-plaintext"
							value="${strPhoneNumber}" name="phoneNumber" readonly>
					</div>
				</div>
				<div class="form-group col-sm-12 mx-auto row">
					<label class="col-sm-4 col-form-label">メールアドレス</label>
					<div class="col-sm-8">
						<input type="email" readonly class="form-control-plaintext"
							value="${strEmail}" name="email" readonly>
					</div>
				</div>
			</div>
		</div>

		<br>

		<div class="card col-sm-8 mx-auto">
			<div class="card-header">ご住所</div>
			<div class="card-body">
				<div class="form-group col-sm-12 mx-auto row">
					<label class="col-sm-4 col-form-label">郵便番号</label>
					<div class="col-sm-8">
						<input type="text" readonly class="form-control-plaintext"
							value="${strPostalCode}" name="postalCode" readonly>
					</div>
				</div>
				<div class="form-group col-sm-12 mx-auto row">
					<label class="col-sm-4 col-form-label">都道府県</label>
					<div class="col-sm-8">
						<input type="text" readonly class="form-control-plaintext"
							value="${strPrefectures}" name="prefectures" readonly>
					</div>
				</div>
				<div class="form-group col-sm-12 mx-auto row">
					<label class="col-sm-4 col-form-label">市町村</label>
					<div class="col-sm-8">
						<input type="text" readonly class="form-control-plaintext"
							value="${strAddress1}" name="address1" readonly>
					</div>
				</div>
				<div class="form-group col-sm-12 mx-auto row">
					<label class="col-sm-4 col-form-label">住所1（番地）</label>
					<div class="col-sm-8">
						<input type="text" readonly class="form-control-plaintext"
							value="${strAddress2}" name="address2" readonly>
					</div>
				</div>
				<div class="form-group col-sm-12 mx-auto row">
					<label class="col-sm-4 col-form-label">住所2（建物名）</label>
					<div class="col-sm-8">
						<input type="text" readonly class="form-control-plaintext"
							 value="${strAddress3}" name="address3" readonly>
					</div>
				</div>
			</div>
		</div>

		<br>

		<div class="card col-sm-8 mx-auto">
			<div class="card-header">パスワード</div>
			<div class="card-body">
				<div class="form-group col-sm-12 mx-auto row">
					<label for="inputPassword" class="col-sm-4 col-form-label">パスワード</label>
					<div class="col-sm-8">
						<input type="password" readonly class="form-control-plaintext"
							 value="${strPassword}" name="password" readonly> パスワードは画面上には表示されません
					</div>
				</div>
			</div>
		</div>

		<br>

		<div class="col-sm-8 mx-auto">内容を確認し、よろしければ登録ボタンを押してください。</div>

		<br>

		<div class="row col-4 mx-auto">
			<div class="col-sm-6">
			<button type="submit" class="btn btn-secondary btn-block" name="confirm_button" value="cancel">入力画面に戻る</button>
			</div>
			<div class="col-sm-6">
				<button type="submit" class="btn btn-primary btn-block" name="confirm_button" value="regist">登録</button>
			</div>
		</div>
	</form>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>
