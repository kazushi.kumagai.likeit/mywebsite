<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>userdereteconfirm</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />

	<div class="col-sm-9 mx-auto mt-sm-4">
		<h3>退会手続き</h3>
		<hr>
	</div>

	<div class="text-center col-sm-9 mt-sm-4 mx-auto">
		<img class="" src="images/caution.png" alt="" width="50" height="">
		<h2 class="my-sm-4">退会の前にご確認ください</h2>
		<div class="alert alert-danger" role="alert">
			退会手続きが完了した時点で、現在保存されている購入履歴やお届け先等の情報は、<br> 全て削除されますのでご注意ください。
		</div>
		<div class="row col-6 mx-auto">
			<div class="col-sm-6">
				<a href="User"><button type="button" class="btn btn-secondary btn-block""col-sm-3">キャンセル</button></a>
			</div>

			<div class="col-sm-6">
				<form action="UserDeleteResult" method="post">
					<input type="hidden" name="id" class="form-control form-control-sm"
						id="colFormLabelSm" value="${userId}" readonly>
					<button type="submit" class="btn btn-dark btn-block""col-sm-3">退会します</button>
				</form>
			</div>
		</div>
	</div>

	<jsp:include page="/baselayout/footer_bottom.jsp" />
</body>
</html>