<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>buyresult</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />

	<div class="text-center col-sm-9 mt-sm-4 mx-auto">
		<h2 class="my-sm-4">ご注文ありがとうございました</h2>
		<p>
			ただいまご注文の確認メールを送らせていただきました。<br>
			万一、ご確認メールが届かない場合は、トラブルの可能性もありますので大変お手数ではございますがもう一度お問い合わせいただくか、お電話にてお問合わせくださいませ。<br>
			今後ともご愛顧賜りますようよろしくお願い申し上げます。
		</p>
		<p class="my-sm-4">ご注文番号：${buy.id}</p>
		<a href="Index"><button type="button" class="col-sm-5 btn btn-dark">トップページへ</button></a>
	</div>

	<jsp:include page="/baselayout/footer_bottom.jsp" />
</body>
</html>