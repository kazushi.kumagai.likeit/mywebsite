<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>userbuyhistory</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />

	<div class="col-sm-9 mx-auto mt-sm-4">
		<h3>マイページ/注文履歴</h3>
		<hr>

		<div class="btn-group col-sm-12" role="group">
			<a href="User" type="button" class="btn btn-outline-dark col-sm-4">会員情報</a>
			<a href="UserUpdate" type="button"
				class="btn btn-outline-dark col-sm-4">会員情報更新</a> <a
				href="UserBuyHistory" type="button"
				class="btn btn-outline-dark col-sm-4">注文履歴</a> <a href="UserDelete"
				type="button" class="btn btn-outline-dark col-sm-4">退会手続き</a>
		</div>
		<table class="table mb-sm-5 my-sm-5" style="font-size: 9pt">
			<tbody>
			<c:forEach var="buyDataList" items="${buyDataList}">
				<tr>
					<td class="" style="width: 70px">
						<p style="font-size: 20px">${buyDataList.formatDate}</p>
						<p class="ml-sm-5" style="font-size: 15px">
							注文ID：${buyDataList.id}<br> 受取人：${user.name}<br> 合計金額：¥${buyDataList.totalPrice}<br>
						</p>
					</td>
					<td class="" style="width: 180px">
						<div class="row mb-sm-3">
							<a href="UserBuyHistoryDetail?id=${buyDataList.id}" type="button" class="btn btn-outline-dark mt-sm-5">注文の詳細</a>
						</div>
					</td>
				</tr>
				</c:forEach>
			</tbody>
		</table>

		<a href="Index"><button type="button" class="col-sm-3 btn btn-dark">トップページへ戻る</button></a>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>