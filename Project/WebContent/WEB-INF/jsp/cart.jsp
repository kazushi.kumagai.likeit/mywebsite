<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>cart</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />

	<div class="col-sm-9 mx-auto my-sm-5">

		<div class="my-sm-3">
			<a href="Index"><button type="button"class="btn btn-outline-secondary col-sm-4 mx-auto">トップページに戻って買い物を続ける</button></a>
		</div>

		<div class="text-center col-sm-9 mt-sm-4 mx-auto">
			<h2 class="my-sm-4">${cartActionMessage}</h2>
		</div>

		<form action="CartUpdate" method="post">
			<table class="table mr-sm-4" style="height: 100px">
				<thead style="font-size: 10pt">
					<tr class="table-secondary">
						<th class="text-center" style="width: 400px">商品名</th>
						<th class="text-center" style="width: 60px">数量</th>
						<th class="text-center" style="width: 150px">小計（税抜）</th>
						<th class="text-center" style="width: 60px"></th>
					</tr>
				</thead>
				<tbody style="font-size: 12px">
					<c:forEach var="item" items="${cart}" varStatus="status">
					<tr>
						<td scope="row" class="align-middle">
							<div class="mr-md-3 pt-3 px-3 py-md-3 row">
								<img class="" src="images/${item.fileName}" alt="" width="150"
									height="100">
								<p class="mt-sm-3 ml-sm-4 py-sm-4">
									${item.name}<br>
								</p>
							</div>
						</td>
						<td class="align-middle">
						<p class="mx-auto" style="font-size: 12px">${item.number}</p>
						<input type="hidden"
							class="form-control col-sm-6 form-control-sm mx-auto" name="number"
							value="${item.number}">
							<input type="hidden" class="form-control form-control-sm col-sm-2"
									name="id" value="${item.id}">
							</td>
						<td class="align-middle">￥${item.price * item.number}</td>
						<td class="align-middle">
							<a href="ItemDelete?id=${item.id}" type="button" class="btn btn-outline-dark mx-auto mt-sm-2">削除</a>
						</td>
					</tr>
					</c:forEach>

				</tbody>
			</table>

			<hr>

			<div class="my-sm-5 px-sm-5 text-right">
				<div class="col-sm-6 ml-auto">
					<div class="row ml-auto">
						<p class="col-sm-4 mr-sm-5">合計金額:</p>
						<p class="text-danger col-sm-6">
						¥${totalPrice.totalPrice}</p>
					</div>
				</div>
			</div>
		</form>
			<hr>

		<c:if test="${cart != '[]'}">
			<% boolean isLogin = session.getAttribute("isLogin")!=null?(boolean) session.getAttribute("isLogin"):false; %>
			<%if(isLogin){ %>
			<div class="my-sm-3 px-sm-5 text-right">
				<a href="BuyInput" type="submit" type="submit" class="btn btn btn-dark col-sm-4 mx-auto">ご購入手続きへ進む</a>
			</div>
			<%}else{%>
				<div class="my-sm-3 px-sm-5 text-right">
				<a href="BuyUserRegist" type="submit" class="btn btn btn-dark col-sm-4 mx-auto">ご購入手続きへ進む</a>
			</div>
			<%}%>
			</c:if>


	</div>

	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>