<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>buyconfirm</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">

</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />

	<div class="col-sm-9 mx-auto">
		<h2 class="my-sm-4">ご注文の商品</h2>

		<form action="BuyResult" method="post">

		<div class="row">


			<table class="table col-sm-7 mr-sm-4" style="height: 100px">
				<thead style="font-size: 10pt">
					<tr class="table-secondary">
						<th class="text-center" style="width: 400px">商品名</th>
						<th class="text-center" style="width: 60px">数量</th>
						<th class="text-center" style="width: 150px">小計（税抜）</th>
					</tr>
				</thead>
				<tbody style="font-size: 12px">
					<c:forEach var="item" items="${cart}" varStatus="status">
				<tr>
					<td scope="row" class="align-middle">
						<div class="mr-md-3 pt-3 px-3 py-md-3 row">
							<img class="" src="images/${item.fileName}" alt="" width="150" height="">
							<p class="mt-sm-3 ml-sm-4 py-sm-4">${item.name}</p>
						</div>
					</td>
					<td class="align-middle">${item.number}</td>
					<td class="align-middle">￥${item.price * item.number}</td>
				</tr>
				<input type="hidden" readonly class="form-control-plaintext"
							 value="${item.id}" name="id" readonly>
				<input type="hidden" readonly class="form-control-plaintext"
							 value="${item.number}" name="amount" readonly>
				</c:forEach>
				</tbody>
			</table>

			<table class="table col-sm-4" style="font-size: 9pt">
				<tbody>
					<tr>
						<th class="table-secondary" style="width: 180px">商品金額合計（税抜き）</th>
						<td class=" text-right" style="width: 180px">¥${totalPrice.totalPrice}</td>
					</tr>
					<tr>
						<th class="table-secondary" style="width: 180px">送料</th>
						<td class=" text-right" style="width: 180px">￥${delivery.price}</td>
					</tr>
					<tr>
						<th class="table-secondary" style="width: 180px">手数料</th>
						<td class=" text-right" style="width: 180px">￥${pay.price}</td>
					</tr>
					<tr>
						<th class="table-secondary" style="width: 180px">消費税</th>
						<td class=" text-right" style="width: 180px">¥${tax.tax}</td>
					</tr>
					<tr>
						<th class="table-secondary" style="width: 180px">合計金額</th>
						<td class=" text-right" style="width: 180px">
						¥${totalPrice.totalPrice + tax.tax + delivery.price + pay.price}
						<input type="hidden" readonly class="form-control-plaintext"
							value="${totalPrice.totalPrice + tax.tax + delivery.price + pay.price}" name="totalPrice" readonly></td>

					</tr>
				</tbody>
			</table>
		</div>

		<hr>

		<h2 class="my-sm-4">配送方法</h2>
		<div class="row">
			<table class="table col-sm-7 mr-sm-4" style="height: 100px">
				<thead style="font-size: 10pt">
					<tr class="table-secondary">
						<th style="width: 400px">ご注文主</th>
					</tr>
				</thead>
				<tbody style="font-size: 10">
					<tr>
						<td scope="row" class="">
							<div class="mr-md-3 pt-3 px-3 py-md-3" style="font-size: 12px">
								<p class="">
									〒 ${user.postalCode} ${user.prefectures}${user.address1}${user.address2}${user.address3}<br> ${user.name} 様<br> TEL:
									${user.phoneNumber}
								</p>
							</div>
						</td>
					</tr>
				</tbody>
			</table>

			<table class="table col-sm-4" style="font-size: 9pt">
				<tbody>
					<tr>
						<th class="table-secondary" style="width: 180px">配送希望日時</th>
						<td class=" text-right" style="width: 180px">
						<input type="text" readonly class="form-control-plaintext"
							value="${deliveryDay}" name="deliveryDay" readonly></td>
					</tr>
					<tr>
						<th class="table-secondary" style="width: 180px">配送希望時間帯</th>
						<td class=" text-right" style="width: 180px">
						<input type="text" readonly class="form-control-plaintext"
							value="${deliveryTime}" name="deliveryTime" readonly></td>
					</tr>
					<tr>
						<th class="table-secondary" style="width: 180px">支払方法</th>
						<td class=" text-right" style="width: 180px">
						<input type="text" readonly class="form-control-plaintext"
							value="${payMethod}" name="payMethod" readonly></td>
					</tr>
				</tbody>
			</table>
		</div>

		<hr>

		<c:if test="${payMethod == 'クレジットカード'}">
		<h2 class="my-sm-4">クレジットカードで支払う</h2>
		<table class="table mb-sm-5 mb-sm-5" style="font-size: 13pt">
				<tbody>
					<c:forEach var="creditcardList" items="${creditCardList}">
						<tr>
							<th class="table-secondary" style="width: 60px">${creditcardList.company}</th>
							<td class="" style="width: 120px">${creditcardList.number}</td>
							<td class="" style="width: 90px">
								<input class="form-check-input" type="radio" name="this" value="${creditcardList.id}" required>
						</tr>
					</c:forEach>
				</tbody>
			</table>


				<c:if test="${creditCardList == '[]'}">
					<p>利用するクレジットカードの情報を下記のフォームに入力して下さい。</p>

					<div class="card">
						<div class="card-body">
							<div class="form-group col-sm-12 mx-auto row">
								<label class="col-sm-2 col-form-label" style="font-size: 12px">カード会社</label>
								<div class="col-sm-8 row">
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="company"
											value="Visa" required> <label
											class="form-check-label" style="font-size: 12px">Visa</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="company"
											value="MasterCard" required> <label
											class="form-check-label" style="font-size: 12px">MasterCard</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="company"
											id="inlineRadio2" value="JCB" required> <label
											class="form-check-label" style="font-size: 12px">JCB</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="company"
											id="inlineRadio2" value="American Express" required>
										<label class="form-check-label" style="font-size: 12px">American
											Express</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="company"
											id="inlineRadio2" value="DinersClub" required> <label
											class="form-check-label" style="font-size: 12px">DinersClub</label>
									</div>
								</div>
							</div>
							<div class="form-group col-sm-12 mx-auto row">
								<label class="col-sm-2 col-form-label" style="font-size: 12px">カード番号</label>
								<div class="col-sm-4 row">
									<input type="tel"
										class="form-control form-control-sm formatter-tel"
										name="cardNumber" required>
								</div>
							</div>
							<div class="form-group col-sm-12 mx-auto row">
								<label class="col-sm-2 col-form-label" style="font-size: 12px">カード名義人</label>
								<div class="col-sm-4 row">
									<input type="text" class="form-control form-control-sm"
										name="name" required>
								</div>
							</div>
							<div class="form-group col-sm-12 mx-auto row">
								<label class="col-sm-2 col-form-label" style="font-size: 12px">セキュリティコード</label>
								<div class="col-sm-2 row">
									<input type="tel" class="form-control form-control-sm"
										name="securityCode" required>
								</div>
								<div style="font-size: 12px">（カード裏面の署名欄などに記載されている３桁または４桁の数字を入力してください）</div>
							</div>
							<div class="form-group col-sm-12 mx-auto row">
								<label class="col-sm-2 col-form-label" style="font-size: 12px">有効期限</label>
								<div class="col-sm-6 row">
									<input type="text"
										class="form-control form-control-sm col-sm-2" name="month"
										required>
									<p>月/</p>
									<input type="text"
										class="form-control form-control-sm col-sm-2" name="year"
										required>
									<p>年</p>
								</div>
							</div>
							<div class="form-group col-sm-12 mx-auto row">
								<label class="col-sm-2 col-form-label" style="font-size: 12px">お支払い区分</label>
								<div class="col-sm-2 row">
									<select class="form-control form-control-sm" name="pay"
										required>
										<option selected>一括払い</option>
										<option>分割払い</option>
										<option>リボ払い</option>
									</select>
								</div>
							</div>
							<div class="form-group col-sm-12 mx-auto row">
								<label class="col-sm-2 col-form-label" style="font-size: 12px">お支払い回数</label>
								<div class="col-sm-2 row">
									<select class="form-control form-control-sm" name="payNumber"
										required>
										<option selected>１回</option>
										<option>２回</option>
										<option>４回</option>
									</select>
								</div>
							</div>
							<div class="form-group" style="font-size: 12px">
								<div class="form-check col-sm-4 mx-auto">
									<input class="form-check-input" type="checkbox" name="chek">
									<label class="form-check-label" for="gridCheck">
										このクレジットカードをアカウントに登録する </label>
								</div>
							</div>
						</div>
					</div>
				</c:if>
			</c:if>

			<br>

			<input type="hidden" readonly class="form-control-plaintext"
				value="${delivery.id}" name="deriveryId" readonly>
			<input type="hidden" readonly class="form-control-plaintext"
				value="${pay.id}" name="payId" readonly>

			<div class="">
				<a href="BuyInput" type="button" class="btn btn-outline-secondary">ご注文方法の指定に戻る</a>
				<button class="btn btn-lg btn-dark btn-block col-sm-4 mx-auto"
					type="submit">注文を確定する</button>
			</div>
		</form>
	</div>

	<br>

	<jsp:include page="/baselayout/footer.jsp" />

</body>
</html>