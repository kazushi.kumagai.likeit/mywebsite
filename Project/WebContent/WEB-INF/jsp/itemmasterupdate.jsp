<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>itemmasterregist</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">
</head>
<body>
	<jsp:include page="/baselayout/header.jsp" />

	<div class="col-sm-9 mx-auto mt-sm-4">
		<h3>商品マスタ/商品更新</h3>
		<hr>

		<form action="ItemMasterUpdate" method="post">
			<div class="form-group">
				<label>商品ID</label> <input type="text" class="form-control" readonly
					name="id" value="${item.id}" required>
			</div>
			<div class="form-group">
				<label>商品名</label> <input type="text" class="form-control"
					name="name" value="${item.name}" required>
			</div>
			<div class="form-group">
				<label>カテゴリ</label> <select class="form-control" name="category" required>
					<option selected>お菓子</option>
					<option>海産物</option>
					<option>農産物</option>
					<option>盛岡冷麺・麺類</option>
					<option>お酒・飲料</option>
					<option>お弁当・パン</option>
					<option>調味料・お漬物</option>
					<option>工芸品</option>
				</select>
			</div>
			<div class="form-row">
				<div class="form-group col-md-6">
					<label>価格</label> <input type="text" class="form-control"
						name="price" value="${item.price}" required>
				</div>
				<div class="form-group col-md-6">
					<label>商品状態</label> <select class="form-control" name="status" required>
						<option value="1">常温商品</option>
						<option value="2">クール便商品</option>
					</select>
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-6">
					<label>在庫</label> <input type="text" class="form-control"
						name="stock" value="${item.stock}" required>
				</div>
				<div class="form-group col-md-6">
					<label>画像データファイル名</label> <input type="text" class="form-control"
						name="fileName" value="${item.fileName}" required>
				</div>
			</div>
			<div class="form-group">
				<label>商品詳細</label>
				<textarea class="form-control" name="detail" rows="3" required>${item.detail}</textarea>
			</div>
			<div class="form-row">
				<div class="form-group col-md-6">
					<label>製造者</label> <input type="text" class="form-control"
						name="manufacture" value="${item.manufacture}" required>
				</div>
				<div class="form-group col-md-6">
					<label>セット内容</label> <input type="text" class="form-control"
						name="amount" value="${item.amount}" required>
				</div>
			</div>
			<div align="center" class="mx-sm-auto">
				<button type="submit" class="btn btn-outline-success col-sm-3 mb-4">更新</button>
			</div>
		</form>
		<a href="ItemMaster" type="buttan"
			class="btn btn-outline-dark col-sm-1 mb-4">戻る</a>
	</div>
	<jsp:include page="/baselayout/footer.jsp" />
</body>
</html>