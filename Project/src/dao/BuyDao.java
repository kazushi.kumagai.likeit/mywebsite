package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Statement;

import beans.BuyDataBeans;

public class BuyDao {

	//購入情報を登録
	public static int insertBuy(String userId, String totalPrice, String deriveryId, String payId, String creditcardId,  String deliveryDay, String deliveryTime){
		Connection conn = null;
		int autoIncKey = -1;

		try {
			conn = DBManager.getConnection();

			String sql = "INSERT INTO t_buy(user_id, total_price, delivery_method_id, pay_method_id, creditcard_id, delivery_date, delivery_time, create_date) VALUES(?,?,?,?,?,?,?,now())";

			//INSERTを実行
			PreparedStatement pStmt = conn.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
			pStmt.setString(1,userId);
			pStmt.setString(2,totalPrice);
			pStmt.setString(3,deriveryId);
			pStmt.setString(4,payId);
			pStmt.setString(5,creditcardId);
			pStmt.setString(6,deliveryDay);
			pStmt.setString(7,deliveryTime);
			pStmt.executeUpdate();

			ResultSet rs = pStmt.getGeneratedKeys();
			if (rs.next()) {
				autoIncKey = rs.getInt(1);
			}
			System.out.println("inserting buy-datas has been completed");

			return autoIncKey;
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
	}
}

	//idに紐づくデータを取得
		public BuyDataBeans findByBuyDetail(int id) {
			Connection conn = null;
			try {
				// データベースへ接続
				conn = DBManager.getConnection();

				// SELECT文を準備
				String sql = "SELECT * FROM t_buy WHERE id = ? ";

				// SELECTを実行し、結果表を取得
				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setInt(1, id);
				ResultSet rs = pStmt.executeQuery();

				// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
				if (!rs.next()) {
					return null;
				}

				// ユーザーデータをインスタンスのフィールドに追加
				int idData = rs.getInt("id");
				int userId = rs.getInt("user_id");
				int totalPrice = rs.getInt("total_price");
				int deliveryMethodId = rs.getInt("delivery_method_id");
				int payMethodId = rs.getInt("pay_method_id");
				String deliveryDate = rs.getString("delivery_date");
				String deliveryTime = rs.getString("delivery_time");
				Date createDate = rs.getDate("create_date");

				//コンストラクタを実行
				return new BuyDataBeans(idData, userId, totalPrice, deliveryMethodId, payMethodId, deliveryDate ,deliveryTime, createDate);

			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		}

		//idに紐づくデータを取得
				public ArrayList<BuyDataBeans> findByBuyHistory(int id) {
					Connection conn = null;
					ArrayList<BuyDataBeans> buyDataList = new ArrayList<BuyDataBeans>();
					try {
						// データベースへ接続
						conn = DBManager.getConnection();

						// SELECT文を準備
						String sql = "SELECT * FROM t_buy"
								+ " JOIN m_delivery_method"
								+ " ON t_buy.delivery_method_id = m_delivery_method.id"
								+ " JOIN m_pay_method"
								+ " ON t_buy.pay_method_id = m_pay_method.id"
								+ " WHERE t_buy.user_id = ?"
								+ " ORDER BY create_date DESC";

						// SELECTを実行し、結果表を取得
						PreparedStatement pStmt = conn.prepareStatement(sql);
						pStmt.setInt(1, id);
						ResultSet rs = pStmt.executeQuery();

						// ユーザーデータをインスタンスのフィールドに追加
						while (rs.next()) {
						int id1 = rs.getInt("t_buy.id");
						int totalPrice = rs.getInt("total_price");
						String deliveryMethodName = rs.getString("m_delivery_method.name");
						int deliveryMethodPrice = rs.getInt("m_delivery_method.price");
						String payMethodName = rs.getString("m_pay_method.name");
						int payMethodPrice = rs.getInt("m_pay_method.price");
						Date createDate = rs.getDate("create_date");

						BuyDataBeans buyData = new BuyDataBeans(id1, totalPrice, deliveryMethodName, deliveryMethodPrice, payMethodName, payMethodPrice, createDate);

						buyDataList.add(buyData);
						}

						return buyDataList;

					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					} finally {
						// データベース切断
						if (conn != null) {
							try {
								conn.close();
							} catch (SQLException e) {
								e.printStackTrace();
								return null;
							}
						}
					}
				}


				//idに紐づくデータを取得
				public BuyDataBeans findBuyDetail(String id) {
					Connection conn = null;
					try {
						// データベースへ接続
						conn = DBManager.getConnection();

						// SELECT文を準備
						String sql = "SELECT * FROM t_buy"
								+ " JOIN m_delivery_method"
								+ " ON t_buy.delivery_method_id = m_delivery_method.id"
								+ " JOIN m_pay_method"
								+ " ON t_buy.pay_method_id = m_pay_method.id"
								+ " WHERE t_buy.id = ?"
								+ " ORDER BY create_date DESC";

						// SELECTを実行し、結果表を取得
						PreparedStatement pStmt = conn.prepareStatement(sql);
						pStmt.setString(1, id);
						ResultSet rs = pStmt.executeQuery();

						// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
						if (!rs.next()) {
							return null;
						}

						// ユーザーデータをインスタンスのフィールドに追加
						int id1 = rs.getInt("t_buy.id");
						int totalPrice = rs.getInt("total_price");
						String deliveryMethodName = rs.getString("m_delivery_method.name");
						int deliveryMethodPrice = rs.getInt("m_delivery_method.price");
						String payMethodName = rs.getString("m_pay_method.name");
						int payMethodPrice = rs.getInt("m_pay_method.price");
						Date createDate = rs.getDate("create_date");

						//コンストラクタを実行
						return new BuyDataBeans(id1, totalPrice, deliveryMethodName, deliveryMethodPrice, payMethodName, payMethodPrice, createDate);

					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					} finally {
						// データベース切断
						if (conn != null) {
							try {
								conn.close();
							} catch (SQLException e) {
								e.printStackTrace();
								return null;
							}
						}
					}
				}

}