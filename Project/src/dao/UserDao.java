package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import beans.UserDataBeans;

public class UserDao {

	/**
	 * ログインIDとパスワードからユーザーIDを取得
	 * @param email
	 * @param password
	 * @return 取得したユーザーID｜取得できなかった場合null
	 */
	public static int findByLoginInfo(String email, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM t_user WHERE email = ? and password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, email);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();
			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return 0;
			}

			int userId = 0;
			// 必要なデータのみインスタンスのフィールドに追加
			userId = rs.getInt("id");
			System.out.println(userId);
			System.out.println("login succeeded");
			return userId;

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("login failed");
			return 0;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}

	//idに紐づくデータを取得
	public UserDataBeans findByUserDetail(int id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM t_user WHERE id = ? ";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// ユーザーデータをインスタンスのフィールドに追加
			int idData = rs.getInt("id");
			String name = rs.getString("name");
			String email = rs.getString("email");
			Date birthDate = rs.getDate("birth_date");
			String phoneNumber = rs.getString("phone_number");
			String postalCode = rs.getString("postal_code");
			String prefectures = rs.getString("prefectures");
			String address1 = rs.getString("address1");
			String address2 = rs.getString("address2");
			String address3 = rs.getString("address3");
			String password = rs.getString("password");
			Date createDate = rs.getDate("create_date");
			Date updateDate = rs.getDate("update_date");
			//コンストラクタを実行
			return new UserDataBeans(idData, name, email, birthDate, phoneNumber, postalCode ,prefectures, address1, address2, address3, password, createDate, updateDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//ユーザーを追加
	public int InsertUserDataBeans(String name, String email, String birthDate, String phoneNumber, String postalCode, String prefectures, String address1, String address2, String address3, String password){
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			//INSERT文を準備
			String sql = "INSERT INTO t_user (name, email, birth_date, phone_number,  postal_code, prefectures, address1, address2, address3, password, create_date, update_date)VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), NOW())";

			//INSERTを実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1,name);
			pStmt.setString(2,email);
			pStmt.setString(3,birthDate);
			pStmt.setString(4,phoneNumber);
			pStmt.setString(5,postalCode);
			pStmt.setString(6,prefectures);
			pStmt.setString(7,address1);
			pStmt.setString(8,address2);
			pStmt.setString(9,address3);
			pStmt.setString(10,password);

			int i = pStmt.executeUpdate();

			return i;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}

	//データを削除
	public int Delete(String id){
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			//UPDATE文を準備
			String sql = "DELETE FROM t_user WHERE id = ?";
			//UPDATEを実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1,id);

			int i = pStmt.executeUpdate();

			return i;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}

	//パスワード以外のデータを更新
    public int UserUpdateNotPass(String name, String email, String birthDate, String phoneNumber, String postalCode, String prefectures, String address1, String address2, String address3 ,int id){
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            //UPDATE文を準備
            String sql = "UPDATE t_user SET name = ?, email = ?, birth_date = ?, phone_number = ?, postal_code = ?, prefectures = ?, address1 = ?, address2 = ?, address3 = ?,update_date = NOW() WHERE id = ?";
            //UPDATEを実行
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1,name);
            pStmt.setString(2,email);
            pStmt.setString(3,birthDate);
            pStmt.setString(4,phoneNumber);
            pStmt.setString(5,postalCode);
            pStmt.setString(6,prefectures);
            pStmt.setString(7,address1);
            pStmt.setString(8,address2);
            pStmt.setString(9,address3);
            pStmt.setInt(10,id);

            int i = pStmt.executeUpdate();

            return i;

        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        }
    }

  //パスワードを更新
    public static int UserPasswordUpdate(String password, int id){
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            //UPDATE文を準備
            String sql = "UPDATE t_user SET password = ? WHERE id = ?";
            //UPDATEを実行
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1,password);
            pStmt.setInt(2,id);

            int i = pStmt.executeUpdate();

            return i;

        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        }
    }

}