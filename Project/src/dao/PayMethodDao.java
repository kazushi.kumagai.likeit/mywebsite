package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import beans.PayMethodDataBeans;

public class PayMethodDao {

//名前から支払い方法を取得
public PayMethodDataBeans getPayMethodDataBeansByName(String PayMethodName){
		Connection conn = null;
		PreparedStatement st = null;
		try {
			conn = DBManager.getConnection();

			st = conn.prepareStatement(
					"SELECT * FROM m_pay_method WHERE name = ?");
			st.setString(1, PayMethodName);

			ResultSet rs = st.executeQuery();

			PayMethodDataBeans pmdb = new PayMethodDataBeans();
			if (rs.next()) {
				pmdb.setId(rs.getInt("id"));
				pmdb.setName(rs.getString("name"));
				pmdb.setPrice(rs.getInt("price"));
			}

			System.out.println("searching DeliveryMethodDataBeans by DeliveryMethodID has been completed");

			return pmdb;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
}