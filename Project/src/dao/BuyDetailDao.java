package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import beans.BuyDetailDataBeans;
import beans.ItemDataBeans;

public class BuyDetailDao {

	//購入情報を登録
		public static int insertBuyDetail(BuyDetailDataBeans bddb){
			Connection conn = null;

			try {
				conn = DBManager.getConnection();

				String sql = "INSERT INTO t_buy_detail(buy_id, item_id, amount) VALUES(?,?,?)";

				//INSERTを実行
				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setInt(1,bddb.getBuyId());
				pStmt.setInt(2,bddb.getItemId());
				pStmt.setInt(3,bddb.getAmount());

				int i = pStmt.executeUpdate();

				return i;

			} catch (SQLException e) {
				e.printStackTrace();
				return 0;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return 0;
					}
				}
			}
		}

		//idに紐づくデータを取得
		public ArrayList<ItemDataBeans> findByBuyItemList(String id) {
			Connection conn = null;
			ArrayList<ItemDataBeans> itemDataList = new ArrayList<ItemDataBeans>();
			try {
				// データベースへ接続
				conn = DBManager.getConnection();

				// SELECT文を準備
				String sql = "SELECT m_item.id,"
						+ " m_item.name,"
						+ " m_item.price,"
						+ " m_item.file_name,"
						+ " t_buy_detail.amount,"
						+ " m_item.stock"
						+ " FROM t_buy_detail"
						+ " JOIN m_item"
						+ " ON t_buy_detail.item_id = m_item.id"
						+ " WHERE t_buy_detail.buy_id = ?";

				// SELECTを実行し、結果表を取得
				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, id);
				ResultSet rs = pStmt.executeQuery();



				// ユーザーデータをインスタンスのフィールドに追加
				while (rs.next()) {
				int id2 = rs.getInt("id");
				String name = rs.getString("name");
				int price = rs.getInt("price");
				String fileName = rs.getString("file_name");
				int amount = rs.getInt("t_buy_detail.amount");
				int stock = rs.getInt("stock");

				ItemDataBeans itemData = new ItemDataBeans(id2, name, price , fileName, amount, stock);

				itemDataList.add(itemData);
				}

				return itemDataList;

			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		}

	}

