package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import beans.BuyDetailDataBeans;
import beans.ItemDataBeans;

public class ItemDao {


	//全ての商品を取得
	public ArrayList<ItemDataBeans> findAll() {
		Connection conn = null;
		ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM m_item ";



			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容をインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String detail = rs.getString("detail");
				int price = rs.getInt("price");
				String fileName = rs.getString("file_name");
				String category = rs.getString("category");
				int stock = rs.getInt("stock");
				int statusId = rs.getInt("status_id");
				String manufacture = rs.getString("manufacture");
				String amount = rs.getString("amount");
				Date createDate = rs.getDate("create_date");
				Date updateDate = rs.getDate("update_date");

				ItemDataBeans item = new ItemDataBeans(id, name, detail, price, fileName, category, stock, statusId, manufacture, amount, createDate, updateDate);

				itemList.add(item);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return itemList;
	}

	//全ての商品総数カウント
	public static double searchItemAllListCount(){
		Connection conn = null;
		double coung = 0.0;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// 条件検索
			String sql = "select count(*) as cnt FROM m_item";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
		return coung;
	}

	//商品を追加
	public int InsertItemDataBeans(String name, String detail, String price, String fileName, String category, String stock, String statusId, String manufacture, String amount){
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			//INSERT文を準備
			String sql = "INSERT INTO m_item (name, detail, price, file_name,  category, stock, status_id, manufacture, amount, create_date, update_date)VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), NOW())";

			//INSERTを実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1,name);
			pStmt.setString(2,detail);
			pStmt.setString(3,price);
			pStmt.setString(4,fileName);
			pStmt.setString(5,category);
			pStmt.setString(6,stock);
			pStmt.setString(7,statusId);
			pStmt.setString(8,manufacture);
			pStmt.setString(9,amount);

			int i = pStmt.executeUpdate();

			return i;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
	}

	//商品マスタ検索
	public ArrayList<ItemDataBeans> searchItemMasterList(String id, String searchWord, String category, String statusId, String stock, String startDate, String endDate){
		Connection conn = null;
		ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// 条件検索
			String sql = "SELECT * FROM m_item WHERE id >= 0 ";
			if(!id.equals("")) {
				sql += " AND id = '" + id + "'";
			}
			if(!searchWord.equals("")) {
				sql += " AND name LIKE '%" + searchWord + "%'";
			}
			if(!category.equals("")) {
				sql += " AND category = '" + category + "'";
			}
			if(!statusId.equals("")) {
				sql += " AND status_id = '" + statusId + "'";
			}
			if(!stock.equals("")) {
				sql += " AND stock " + stock + "";
			}
			if(!startDate.equals("")) {
				sql += " AND create_date >= '" + startDate + "'";
			}
			if(!endDate.equals("")) {
				sql += " AND create_date <= '" + endDate + "'";
			}

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);


			while (rs.next()) {
				int id1 = rs.getInt("id");
				String name = rs.getString("name");
				int price = rs.getInt("price");
				String fileName = rs.getString("file_name");
				int stock1 = rs.getInt("stock");
				ItemDataBeans item = new ItemDataBeans(id1, name, price, fileName, stock1);

				itemList.add(item);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return itemList;
	}

	//商品マスタ総数カウント
	public static double searchItemMasterListCount(String id, String searchWord, String category, String statusId, String stock, String startDate, String endDate){
		Connection conn = null;
		double coung = 0.0;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// 条件検索
			String sql = "select count(*) as cnt FROM m_item WHERE id >= 0 ";
			if(!id.equals("")) {
				sql += " AND id = '" + id + "'";
			}
			if(!searchWord.equals("")) {
				sql += " AND name LIKE '%" + searchWord + "%'";
			}
			if(!category.equals("")) {
				sql += " AND category = '" + category + "'";
			}
			if(!statusId.equals("")) {
				sql += " AND status_id = '" + statusId + "'";
			}
			if(!stock.equals("")) {
				sql += " AND stock " + stock + "";
			}
			if(!startDate.equals("")) {
				sql += " AND create_date >= '" + startDate + "'";
			}
			if(!endDate.equals("")) {
				sql += " AND create_date <= '" + endDate + "'";
			}

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return 0;
				}
			}
		}
		return coung;
	}

	//idに紐づくデータを取得
	public static ItemDataBeans findByItemDetail(String id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM m_item "
					+ "JOIN item_status "
					+ "ON m_item.status_id = item_status.id "
					+ "WHERE m_item.id = ? ";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// ユーザーデータをインスタンスのフィールドに追加
			int id1 = rs.getInt("m_item.id");
			String name = rs.getString("name");
			String detail = rs.getString("detail");
			int price = rs.getInt("price");
			String fileName = rs.getString("file_name");
			String category = rs.getString("category");
			int stock = rs.getInt("stock");
			int statusId = rs.getInt("status_id");
			String manufacture = rs.getString("manufacture");
			String amount = rs.getString("amount");
			Date createDate = rs.getDate("create_date");
			Date updateDate = rs.getDate("update_date");
			String statusName = rs.getString("status_name");

			System.out.println(id);
			System.out.println(name);
			System.out.println(detail);
			System.out.println(price);
			System.out.println(fileName);
			System.out.println(category);

			//コンストラクタを実行
			return new ItemDataBeans(id1, name, detail, price, fileName, category, stock, statusId, manufacture, amount, createDate, updateDate, statusName);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//データを更新
    public int ItemUpdate(String id, String name, String detail, String price, String fileName, String stock, String status, String manufacture, String amount, String category){
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            //UPDATE文を準備
            String sql = "UPDATE m_item SET name = ?, detail = ?, price = ?, file_name = ?, stock = ?, status_id = ?, manufacture = ?, amount = ?, category = ?, update_date = NOW() WHERE id = ?";
            //UPDATEを実行
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1,name);
            pStmt.setString(2,detail);
            pStmt.setString(3,price);
            pStmt.setString(4,fileName);
            pStmt.setString(5,stock);
            pStmt.setString(6,status);
            pStmt.setString(7,manufacture);
            pStmt.setString(8,amount);
            pStmt.setString(9,category);
            pStmt.setString(10,id);

            int i = pStmt.executeUpdate();

            return i;

        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        }
    }

  //データを削除
  	public int ItemDelete(String id){
  		Connection conn = null;
  		try {
  			// データベースへ接続
  			conn = DBManager.getConnection();

  			//UPDATE文を準備
  			String sql = "DELETE FROM m_item WHERE id = ?";
  			//UPDATEを実行
  			PreparedStatement pStmt = conn.prepareStatement(sql);
  			pStmt.setString(1,id);

  			int i = pStmt.executeUpdate();

  			return i;

  		} catch (SQLException e) {
  			e.printStackTrace();
  			return 0;
  		} finally {
  			// データベース切断
  			if (conn != null) {
  				try {
  					conn.close();
  				} catch (SQLException e) {
  					e.printStackTrace();
  					return 0;
  				}
  			}
  		}
  	}

  //商品検索
  	public ArrayList<ItemDataBeans> searchItemList(String searchWord, String category ,int pageNum, int pageMaxItemCount){
  		Connection conn = null;
  		ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();
  		int startiItemNum = (pageNum - 1) * pageMaxItemCount;

  		try {
  			// データベースへ接続
  			conn = DBManager.getConnection();

  			// 条件検索
  			String sql = "SELECT * FROM m_item WHERE id >= 0 ";
  			if(!searchWord.equals("")) {
  				sql += " AND name LIKE '%" + searchWord + "%'";
  			}
  			if(!category.equals("")) {
  				sql += " AND category = '" + category + "'";
  			}
  			sql += " ORDER BY id ASC LIMIT " + startiItemNum + ", " + pageMaxItemCount + "";
  			// SELECTを実行し、結果表を取得
  			Statement stmt = conn.createStatement();
  			ResultSet rs = stmt.executeQuery(sql);


  			while (rs.next()) {
  				int id1 = rs.getInt("id");
  				String name = rs.getString("name");
  				int price = rs.getInt("price");
  				String fileName = rs.getString("file_name");
  				String category1 = rs.getString("category");
  				int statusId = rs.getInt("status_id");
  				int stock1 = rs.getInt("stock");
  				ItemDataBeans item = new ItemDataBeans(id1, name, price, fileName, stock1,category1,statusId);

  				itemList.add(item);
  			}
  		} catch (SQLException e) {
  			e.printStackTrace();
  			return null;
  		} finally {
  			// データベース切断
  			if (conn != null) {
  				try {
  					conn.close();
  				} catch (SQLException e) {
  					e.printStackTrace();
  					return null;
  				}
  			}
  		}
  		return itemList;
  	}

  //商品マスタ総数カウント
  	public static double searchItemListCount(String searchWord, String category){
  		Connection conn = null;
  		double coung = 0.0;
  		try {
  			// データベースへ接続
  			conn = DBManager.getConnection();

  			// 条件検索
  			String sql = "select count(*) as cnt FROM m_item WHERE id >= 0 ";
  			if(!searchWord.equals("")) {
  				sql += " AND name LIKE '%" + searchWord + "%'";
  			}
  			if(!category.equals("")) {
  				sql += " AND category = '" + category + "'";
  			}

  			// SELECTを実行し、結果表を取得
  			Statement stmt = conn.createStatement();
  			ResultSet rs = stmt.executeQuery(sql);

  			while (rs.next()) {
  				coung = Double.parseDouble(rs.getString("cnt"));
  			}
  		} catch (SQLException e) {
  			e.printStackTrace();
  			return 0;
  		} finally {
  			// データベース切断
  			if (conn != null) {
  				try {
  					conn.close();
  				} catch (SQLException e) {
  					e.printStackTrace();
  					return 0;
  				}
  			}
  		}
  		return coung;
  	}


  	//同じカテゴリの商品をランダムで5件取得
  	public static ArrayList<ItemDataBeans> getRandItem(String category){
  		Connection conn = null;
  		ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM m_item WHERE category = ? ORDER BY RAND() LIMIT 5 ";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, category);
			ResultSet rs = pStmt.executeQuery();


  			while (rs.next()) {
  				int id1 = rs.getInt("id");
  				String name = rs.getString("name");
  				int price = rs.getInt("price");
  				String fileName = rs.getString("file_name");
  				String category1 = rs.getString("category");
  				int statusId = rs.getInt("status_id");
  				int stock1 = rs.getInt("stock");
  				ItemDataBeans item = new ItemDataBeans(id1, name, price, fileName, stock1,category1, statusId);

  				itemList.add(item);
  			}
  		} catch (SQLException e) {
  			e.printStackTrace();
  			return null;
  		} finally {
  			// データベース切断
  			if (conn != null) {
  				try {
  					conn.close();
  				} catch (SQLException e) {
  					e.printStackTrace();
  					return null;
  				}
  			}
  		}
  		return itemList;
  	}

  	 //在庫を更新
    public static int UpdateItemStock(BuyDetailDataBeans bddb){
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            //UPDATE文を準備
            String sql = "UPDATE m_item SET stock = stock-? WHERE id = ?";
            //UPDATEを実行
            PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1,bddb.getAmount());
			pStmt.setInt(2,bddb.getItemId());

            int i = pStmt.executeUpdate();

            return i;

        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        }
    }

}