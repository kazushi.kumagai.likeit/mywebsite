package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import beans.UserCreditCardMethodDataBeans;

public class CreditCardDao {

	//ユーザーidに紐づくデータを取得
	public ArrayList<UserCreditCardMethodDataBeans> findByCreditCardDetail(int id) {
		Connection conn = null;
		ArrayList<UserCreditCardMethodDataBeans> UserCreditCardMethodDataBeansList = new ArrayList<UserCreditCardMethodDataBeans>();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user_creditcard_method WHERE user_id = ?";


			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);
			ResultSet rs = pStmt.executeQuery();

			// ユーザーデータをインスタンスのフィールドに追加

			while (rs.next()) {
				int CreditCardId = rs.getInt("id");
				int userId = rs.getInt("user_id");
				String company = rs.getString("company");
				String number = rs.getString("number");
				String name = rs.getString("name");
				int month = rs.getInt("month");
				int year = rs.getInt("year");

				UserCreditCardMethodDataBeans CreditCardData = new UserCreditCardMethodDataBeans(CreditCardId, userId, company, number, name, month, year);

				UserCreditCardMethodDataBeansList.add(CreditCardData);
			}

			System.out.println("searching all UserCreditCardMethodDataBeans has been completed");

			return UserCreditCardMethodDataBeansList;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//idに紐づくデータを取得
		public UserCreditCardMethodDataBeans CreditCardDetail(String id) {
			Connection conn = null;
			try {
				// データベースへ接続
				conn = DBManager.getConnection();

				// SELECT文を準備
				String sql = "SELECT * FROM user_creditcard_method WHERE id = ?";


				// SELECTを実行し、結果表を取得
				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, id);
				ResultSet rs = pStmt.executeQuery();

				if (!rs.next()) {
					return null;
				}

				// ユーザーデータをインスタンスのフィールドに追加
					int CreditCardId = rs.getInt("id");
					int userId = rs.getInt("user_id");
					String company = rs.getString("company");
					String number = rs.getString("number");
					String name = rs.getString("name");
					int month = rs.getInt("month");
					int year = rs.getInt("year");

					System.out.println("searching UserCreditCardMethodDataBeans has been completed");

					return new UserCreditCardMethodDataBeans(CreditCardId, userId, company, number, name, month, year);

			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		}

	//データを更新
    public static int CreditCardUpdate(String company, String number, String month, String year, String id){
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            //UPDATE文を準備
            String sql = "UPDATE user_creditcard_method SET company = ?, number = ?, month = ?, year = ? WHERE id = ?";
            //UPDATEを実行
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1,company);
            pStmt.setString(2,number);
            pStmt.setString(3,month);
            pStmt.setString(4,year);
            pStmt.setString(5,id);

            int i = pStmt.executeUpdate();

            System.out.println("CreditCardData Updated ");
            return i;

        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        }
    }

  //データを削除
  	public static int Delete(String id){
  		Connection conn = null;
  		try {
  			// データベースへ接続
  			conn = DBManager.getConnection();

  			//UPDATE文を準備
  			String sql = "DELETE FROM user_creditcard_method WHERE id = ?";
  			//UPDATEを実行
  			PreparedStatement pStmt = conn.prepareStatement(sql);
  			pStmt.setString(1,id);

  			int i = pStmt.executeUpdate();

  			System.out.println("CreditCardData Deleted ");
  			return i;

  		} catch (SQLException e) {
  			e.printStackTrace();
  			return 0;
  		} finally {
  			// データベース切断
  			if (conn != null) {
  				try {
  					conn.close();
  				} catch (SQLException e) {
  					e.printStackTrace();
  					return 0;
  				}
  			}
  		}
  	}



  	//購入の際のクレジットカード情報を登録
  		public static int insertBuyCreditCard(int buyId, String userId, String company, String cardNumber, String name, String securityCode, String month, String year, String pay, String payNumber){
  			Connection conn = null;

  			try {
  				conn = DBManager.getConnection();

  				String sql = "INSERT INTO buy_creditcard_method(buy_id, user_id, company, number, name, security_code, month, year, payment_category, payment_number) VALUES(?,?,?,?,?,?,?,?,?,?)";

  				//INSERTを実行
  				PreparedStatement pStmt = conn.prepareStatement(sql);
  				pStmt.setInt(1,buyId);
  				pStmt.setString(2,userId);
  				pStmt.setString(3,company);
  				pStmt.setString(4,cardNumber);
  				pStmt.setString(5,name);
  				pStmt.setString(6,securityCode);
  				pStmt.setString(7,month);
  				pStmt.setString(8,year);
  				pStmt.setString(9,pay);
  				pStmt.setString(10,payNumber);

  				int i = pStmt.executeUpdate();

  				return i;

  			} catch (SQLException e) {
  				e.printStackTrace();
  				return 0;
  			} finally {
  				// データベース切断
  				if (conn != null) {
  					try {
  						conn.close();
  					} catch (SQLException e) {
  						e.printStackTrace();
  						return 0;
  					}
  				}
  			}
  		}

  	//クレジットカード情報を登録
  		public static int insertCreditCard(String userId, String company, String cardNumber, String name, String month, String year){
  			Connection conn = null;

  			try {
  				conn = DBManager.getConnection();

  				String sql = "INSERT INTO user_creditcard_method(user_id, company, number, name, month, year) VALUES(?,?,?,?,?,?)";

  				//INSERTを実行
  				PreparedStatement pStmt = conn.prepareStatement(sql);
  				pStmt.setString(1,userId);
  				pStmt.setString(2,company);
  				pStmt.setString(3,cardNumber);
  				pStmt.setString(4,name);
  				pStmt.setString(5,month);
  				pStmt.setString(6,year);

  				int i = pStmt.executeUpdate();

  				return i;

  			} catch (SQLException e) {
  				e.printStackTrace();
  				return 0;
  			} finally {
  				// データベース切断
  				if (conn != null) {
  					try {
  						conn.close();
  					} catch (SQLException e) {
  						e.printStackTrace();
  						return 0;
  					}
  				}
  			}
  		}

}

