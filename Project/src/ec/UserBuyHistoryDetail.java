package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.ItemDataBeans;
import beans.UserDataBeans;
import dao.BuyDao;
import dao.BuyDetailDao;
import dao.UserDao;

/**
 * Servlet implementation class UserBuyHistoryDetail
 */
@WebServlet("/UserBuyHistoryDetail")
public class UserBuyHistoryDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserBuyHistoryDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

        //セッションに保存したユーザーIDを取得
        int userId = (int) session.getAttribute("userId");

     // URLからGETパラメータとしてIDを受け取る
     	String id = request.getParameter("id");

		// ユーザ一覧情報を取得
		UserDao userDao = new UserDao();
		UserDataBeans user = userDao.findByUserDetail(userId);
		request.setAttribute("user", user);

		// 購入情報を取得
		BuyDao BuyDao = new BuyDao();
		BuyDataBeans buyData = BuyDao.findBuyDetail(id);
		request.setAttribute("buyData", buyData);

		// 購入情報を取得
		BuyDetailDao ItemDao = new BuyDetailDao();
		ArrayList<ItemDataBeans> ItemDataList = ItemDao.findByBuyItemList(id);
		request.setAttribute("ItemDataList", ItemDataList);

		// 購入履歴詳細のjspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userbuyhistorydetail.jsp");
				dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
