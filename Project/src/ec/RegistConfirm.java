package ec;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import dao.UserDao;

/**
 * Servlet implementation class RegistConfirm
 */
@WebServlet("/RegistConfirm")
public class RegistConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegistConfirm() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
				request.setCharacterEncoding("UTF-8");

				// リクエストパラメータの入力項目を取得
				String name = request.getParameter("name");
				String birthDate = request.getParameter("birthDate");
				String phoneNumber = request.getParameter("phoneNumber");
				String email = request.getParameter("email");
				String postalCode = request.getParameter("postalCode");
				String prefectures = request.getParameter("prefectures");
				String address1 = request.getParameter("address1");
				String address2 = request.getParameter("address2");
				String address3 = request.getParameter("address3");
				String password = request.getParameter("password");

				String confirmed = request.getParameter("confirm_button");

				UserDao userDao = new UserDao();

				//暗号化処理
				//ハッシュを生成したい元の文字列
				String source = password;
				//ハッシュ生成前にバイト配列に置き換える際のCharset
				Charset charset = StandardCharsets.UTF_8;
				//ハッシュアルゴリズム
				String algorithm = "MD5";

				//ハッシュ生成処理
				byte[] bytes = null;
				try {
					bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
				} catch (NoSuchAlgorithmException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
				String result = DatatypeConverter.printHexBinary(bytes);
				//標準出力
				System.out.println(result);


				switch (confirmed) {
				//入力画面に戻る
				case "cancel":
					request.setAttribute("strName", name);
					request.setAttribute("strBirthDate", birthDate);
					request.setAttribute("strPhoneNumber", phoneNumber);
					request.setAttribute("strEmail", email);
					request.setAttribute("strPostalCode", postalCode);
					request.setAttribute("strPrefectures", prefectures);
					request.setAttribute("strAddress1", address1);
					request.setAttribute("strAddress2", address2);
					request.setAttribute("strAddress3", address3);
					RequestDispatcher dispatchercancel = request.getRequestDispatcher("/WEB-INF/jsp/regist.jsp");
					dispatchercancel.forward(request, response);
					break;


				//登録確定
				case "regist":
				// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
				int user = userDao.InsertUserDataBeans(name, email, birthDate, phoneNumber, postalCode, prefectures, address1, address2, address3, result);

				 //登録エラーが発生した場合
				if (user == 0) {
					request.setAttribute("errMsg", "入力したメールアドレスは既に利用されています");
					request.setAttribute("strName", name);
					request.setAttribute("strBirthDate", birthDate);
					request.setAttribute("strPhoneNumber", phoneNumber);
					request.setAttribute("strEmail", email);
					request.setAttribute("strPostalCode", postalCode);
					request.setAttribute("strPrefectures", prefectures);
					request.setAttribute("strAddress1", address1);
					request.setAttribute("strAddress2", address2);
					request.setAttribute("strAddress3", address3);
				// ユーザ新規登録画面に戻る
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/regist.jsp");
					dispatcher.forward(request, response);
					return;
				}

				//正常に登録できた場合、トップ画面にリダイレクト
				response.sendRedirect("Login");

		}
	}
}


