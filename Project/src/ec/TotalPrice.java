package ec;

import java.util.ArrayList;

import beans.ItemDataBeans;

public class TotalPrice{

	public static int getTotalItemPrice(ArrayList<ItemDataBeans> items) {
		int total = 0;
		for (ItemDataBeans item : items) {
			total = item.getPrice() * item.getNumber();
		}
		return total;
	}

	public static int getAllTotalItemPrice(ArrayList<ItemDataBeans> items) {
		int total = 0;
		for (ItemDataBeans item : items) {
			total += item.getPrice() * item.getNumber();
		}
		return total;
	}

	public static int getTax(ArrayList<ItemDataBeans> items) {
		int total = 0;
		for (ItemDataBeans item : items) {
			total += item.getPrice() * item.getNumber();
		}
		int tax = total * 10 / 100;
		return tax;
	}
}
