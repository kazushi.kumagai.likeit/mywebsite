package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.ItemDataBeans;
import dao.ItemDao;

/**
 * Servlet implementation class ItemMasterRegistConfirm
 */
@WebServlet("/ItemMasterRegistConfirm")
public class ItemMasterRegistConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemMasterRegistConfirm() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String name = request.getParameter("name");
		String detail = request.getParameter("detail");
		String price = request.getParameter("price");
		String fileName = request.getParameter("fileName");
		String category = request.getParameter("category");
		String stock = request.getParameter("stock");
		String statusId = request.getParameter("statusId");
		String manufacture = request.getParameter("manufacture");
		String amount = request.getParameter("amount");

		ItemDataBeans itemdb = new ItemDataBeans();
		itemdb.setName(name);
		itemdb.setDetail(detail);
		itemdb.setPrice(Integer.parseInt(price));
		itemdb.setFileName(fileName);
		itemdb.setCategory(category);
		itemdb.setStock(Integer.parseInt(stock));
		itemdb.setStatusId(Integer.parseInt(statusId));
		itemdb.setManufacture(manufacture);
		itemdb.setAmount(amount);



		String confirmed = request.getParameter("confirm_button");

		switch (confirmed) {
		//入力画面に戻る
		case "cancel":
			request.setAttribute("item", itemdb);
			RequestDispatcher dispatchercancel = request.getRequestDispatcher("/WEB-INF/jsp/itemmasterregist.jsp");
			dispatchercancel.forward(request, response);
			break;

		//登録確定
		case "regist":
			ItemDao itemDao = new ItemDao();
			int item = itemDao.InsertItemDataBeans(name, detail, price, fileName, category, stock, statusId, manufacture, amount);
			//正常に登録できた場合、ユーザー画面へ

			//メッセージをセット
			request.setAttribute("msg", "商品を登録しました");

			// 商品一覧情報を取得
			ArrayList<ItemDataBeans> itemList = itemDao.findAll();
			request.setAttribute("itemList", itemList);

			//1ページに表示する商品数
			int PAGE_MAX_ITEM_COUNT = 10;

			// 検索ワードに対しての総ページ数を取得
			double itemCount = ItemDao.searchItemAllListCount();
			int pageMax = (int) Math.ceil(itemCount / PAGE_MAX_ITEM_COUNT);

			//総アイテム数
			request.setAttribute("itemCount", (int) itemCount);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemmaster.jsp");
			dispatcher.forward(request, response);
			break;
		}

}}
