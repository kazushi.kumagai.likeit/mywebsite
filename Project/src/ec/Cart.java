package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.ItemDataBeans;

/**
 * Servlet implementation class Cart
 */
@WebServlet("/Cart")
public class Cart extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Cart() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		ArrayList<ItemDataBeans> cart = (ArrayList<ItemDataBeans>) session.getAttribute("cart");
		System.out.println(cart);
		//セッションにカートがない場合カートを作成
		if (cart == null) {
			cart = new ArrayList<ItemDataBeans>();

			session.setAttribute("cart", cart);
		}

		String cartActionMessage = "";
		//カートに商品が入っていないなら
		if(cart.size() == 0) {
			cartActionMessage = "カートに商品がありません";
		}

		//合計金額
		int totalPrice = TotalPrice.getAllTotalItemPrice(cart);
		BuyDataBeans cartItem = new BuyDataBeans();
		cartItem.setTotalPrice(totalPrice);
		request.setAttribute("totalPrice", cartItem);

		// カートのjspにフォワード
		request.setAttribute("cartActionMessage", cartActionMessage);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/cart.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
