package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.ItemDataBeans;
import dao.ItemDao;

/**
 * Servlet implementation class ItemMaster
 */
@WebServlet("/ItemMaster")
public class ItemMaster extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemMaster() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

		// 商品一覧情報を取得
		ItemDao itemDao = new ItemDao();
		ArrayList<ItemDataBeans> itemList = itemDao.findAll();
		request.setAttribute("itemList", itemList);

		//1ページに表示する商品数
		int PAGE_MAX_ITEM_COUNT = 10;

		// 検索ワードに対しての総ページ数を取得
		double itemCount = ItemDao.searchItemAllListCount();
		int pageMax = (int) Math.ceil(itemCount / PAGE_MAX_ITEM_COUNT);

		//総アイテム数
		request.setAttribute("itemCount", (int) itemCount);


		// 商品マスタ画面にフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemmaster.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String category = request.getParameter("category");
		String statusId = request.getParameter("statusId");
		String stock = request.getParameter("stock");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");

		//検査項目から商品を取得
		ItemDao itemDao = new ItemDao();
		ArrayList<ItemDataBeans> itemList = itemDao.searchItemMasterList(id, name, category, statusId, stock, startDate, endDate);
		request.setAttribute("itemList", itemList);

		//1ページに表示する商品数
		int PAGE_MAX_ITEM_COUNT = 10;

		// 検索ワードに対しての総ページ数を取得
		double itemCount = ItemDao.searchItemMasterListCount(id, name, category, statusId, stock, startDate, endDate);
		int pageMax = (int) Math.ceil(itemCount / PAGE_MAX_ITEM_COUNT);

		//総アイテム数
		request.setAttribute("itemCount", (int) itemCount);

		// 商品マスタ画面にフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemmaster.jsp");
		dispatcher.forward(request, response);

	}

}
