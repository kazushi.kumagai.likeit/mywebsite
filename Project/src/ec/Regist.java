package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;

/**
 * Servlet implementation class Regist
 */
@WebServlet("/Regist")
public class Regist extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Regist() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// ユーザ新規登録のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/regist.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		String phoneNumber = request.getParameter("phoneNumber");
		String email = request.getParameter("email");
		String postalCode = request.getParameter("postalCode");
		String prefectures = request.getParameter("prefectures");
		String address1 = request.getParameter("address1");
		String address2 = request.getParameter("address2");
		String address3 = request.getParameter("address3");
		String password = request.getParameter("password");
		String password_again = request.getParameter("password_again");
		UserDao userDao = new UserDao();

		request.setAttribute("strName", name);
		request.setAttribute("strBirthDate", birthDate);
		request.setAttribute("strPhoneNumber", phoneNumber);
		request.setAttribute("strEmail", email);
		request.setAttribute("strPostalCode", postalCode);
		request.setAttribute("strPrefectures", prefectures);
		request.setAttribute("strAddress1", address1);
		request.setAttribute("strAddress2", address2);
		request.setAttribute("strAddress3", address3);

		//確認用パスワードが異なる、もしくは入力欄に未入力の項目があった場合
			if (!password.equals(password_again)
				|| name.equals("")
				|| birthDate.equals("")
				|| phoneNumber.equals("")
				|| email.equals("")
				|| postalCode.equals("")
				|| prefectures.equals("")
				|| address1.equals("")
				|| address2.equals("")
				|| password.equals("")
				|| password_again.equals("")) {
			// リクエストスコープにエラーメッセージをセット、パスワード以外の入力した値を保存する
				request.setAttribute("errMsg", "入力された内容は正しくありません");
				request.setAttribute("strName", name);
				request.setAttribute("strBirthDate", birthDate);
				request.setAttribute("strPhoneNumber", phoneNumber);
				request.setAttribute("strEmail", email);
				request.setAttribute("strPostalCode", postalCode);
				request.setAttribute("strPrefectures", prefectures);
				request.setAttribute("strAddress1", address1);
				request.setAttribute("strAddress2", address2);
				request.setAttribute("strAddress3", address3);
			// ユーザ新規登録画面に戻る
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/regist.jsp");
				dispatcher.forward(request, response);
				return;
			}

			//正常に入力された場合
			// リクエストスコープにユーザー情報をセット
			request.setAttribute("strName", name);
			request.setAttribute("strBirthDate", birthDate);
			request.setAttribute("strPhoneNumber", phoneNumber);
			request.setAttribute("strEmail", email);
			request.setAttribute("strPostalCode", postalCode);
			request.setAttribute("strPrefectures", prefectures);
			request.setAttribute("strAddress1", address1);
			request.setAttribute("strAddress2", address2);
			request.setAttribute("strAddress3", address3);
			request.setAttribute("strPassword", password);

			// 登録確認画面にフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/registconfirm.jsp");
			dispatcher.forward(request, response);

	}
}
