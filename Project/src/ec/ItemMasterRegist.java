package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ItemMasterRegist
 */
@WebServlet("/ItemMasterRegist")
public class ItemMasterRegist extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemMasterRegist() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String name = request.getParameter("name");
		String detail = request.getParameter("detail");
		String price = request.getParameter("price");
		String fileName = request.getParameter("fileName");
		String category = request.getParameter("category");
		String stock = request.getParameter("stock");
		String statusId = request.getParameter("statusId");
		String manufacture = request.getParameter("manufacture");
		String amount = request.getParameter("amount");

		// リクエストスコープにユーザー情報をセット
		request.setAttribute("strName", name);
		request.setAttribute("strDetail", detail);
		request.setAttribute("strPrice", price);
		request.setAttribute("strFileName", fileName);
		request.setAttribute("strCategory", category);
		request.setAttribute("strStock", stock);
		request.setAttribute("strStatusId", statusId);
		request.setAttribute("strManufacture", manufacture);
		request.setAttribute("strAmount", amount);

		// 商品マスタ新規登録にフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemmasterregist.jsp");
				dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
				request.setCharacterEncoding("UTF-8");

				// リクエストパラメータの入力項目を取得
				String name = request.getParameter("name");
				String detail = request.getParameter("detail");
				String price = request.getParameter("price");
				String fileName = request.getParameter("fileName");
				String category = request.getParameter("category");
				String stock = request.getParameter("stock");
				String statusId = request.getParameter("statusId");
				String manufacture = request.getParameter("manufacture");
				String amount = request.getParameter("amount");

				// リクエストスコープにユーザー情報をセット
				request.setAttribute("strName", name);
				request.setAttribute("strDetail", detail);
				request.setAttribute("strPrice", price);
				request.setAttribute("strFileName", fileName);
				request.setAttribute("strCategory", category);
				request.setAttribute("strStock", stock);
				request.setAttribute("strStatusId", statusId);
				request.setAttribute("strManufacture", manufacture);
				request.setAttribute("strAmount", amount);

				// 登録確認画面にフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemmasterregistconfirm.jsp");
				dispatcher.forward(request, response);
	}

}
