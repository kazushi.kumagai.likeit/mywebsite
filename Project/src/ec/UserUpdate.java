package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserCreditCardMethodDataBeans;
import beans.UserDataBeans;
import dao.CreditCardDao;
import dao.UserDao;

/**
 * Servlet implementation class UserUpdate
 */
@WebServlet("/UserUpdate")
public class UserUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

        //セッションに保存したユーザーIDを取得
        HttpSession session = request.getSession();
        int userId = (int) session.getAttribute("userId");

		// ユーザ一覧情報を取得
		UserDao userDao = new UserDao();
		UserDataBeans user = userDao.findByUserDetail(userId);
		request.setAttribute("user", user);

		// ユーザ情報更新のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		String phoneNumber = request.getParameter("phoneNumber");
		String email = request.getParameter("email");
		String postalCode = request.getParameter("postalCode");
		String prefectures = request.getParameter("prefectures");
		String address1 = request.getParameter("address1");
		String address2 = request.getParameter("address2");
		String address3 = request.getParameter("address3");

		UserDao userDao = new UserDao();

		//セッションに保存したユーザーIDを取得
		HttpSession session = request.getSession();
        int userId = (int) session.getAttribute("userId");

//		//暗号化処理
//		//ハッシュを生成したい元の文字列
//		String source = password;
//		//ハッシュ生成前にバイト配列に置き換える際のCharset
//		Charset charset = StandardCharsets.UTF_8;
//		//ハッシュアルゴリズム
//		String algorithm = "MD5";
//
//		//ハッシュ生成処理
//		byte[] bytes = null;
//		try {
//			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
//		} catch (NoSuchAlgorithmException e) {
//			// TODO 自動生成された catch ブロック
//			e.printStackTrace();
//		}
//		String result = DatatypeConverter.printHexBinary(bytes);
//		//標準出力
//		System.out.println(result);

		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		int user = userDao.UserUpdateNotPass(name, email, birthDate, phoneNumber, postalCode, prefectures, address1, address2, address3, userId);

		 //登録エラーが発生した場合
		if (user == 0) {
			request.setAttribute("errMsg", "未入力の項目があります");
			request.setAttribute("strName", name);
			request.setAttribute("strBirthDate", birthDate);
			request.setAttribute("strPhoneNumber", phoneNumber);
			request.setAttribute("strEmail", email);
			request.setAttribute("strPostalCode", postalCode);
			request.setAttribute("strPrefectures", prefectures);
			request.setAttribute("strAddress1", address1);
			request.setAttribute("strAddress2", address2);
			request.setAttribute("strAddress3", address3);
		// ユーザ新規登録画面に戻る
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//正常に登録できた場合、ユーザー画面へ

		//メッセージをセット
		request.setAttribute("msg", "ユーザー情報を更新しました");

		//ユーザー情報を取得
		UserDataBeans userData = userDao.findByUserDetail(userId);
		request.setAttribute("user", userData);

		// クレジットカード情報を取得
		CreditCardDao CreditCardDao = new CreditCardDao();
		ArrayList<UserCreditCardMethodDataBeans> creditCardList = CreditCardDao.findByCreditCardDetail(userId);
		request.setAttribute("creditCardList", creditCardList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user.jsp");
		dispatcher.forward(request, response);

}

	}


