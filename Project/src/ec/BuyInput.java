package ec;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.ItemDataBeans;
import beans.UserDataBeans;
import dao.UserDao;

/**
 * Servlet implementation class BuyInput
 */
@WebServlet("/BuyInput")
public class BuyInput extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyInput() {
        super();
        // TODO Auto-generated constructor stub
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();

		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

        //セッションに保存したユーザーIDを取得
        int userId = (int) session.getAttribute("userId");

		// ユーザ一覧情報を取得
		UserDao userDao = new UserDao();
		UserDataBeans user = userDao.findByUserDetail(userId);
		request.setAttribute("user", user);

		//カート情報を取得
		ArrayList<ItemDataBeans> cart = (ArrayList<ItemDataBeans>) session.getAttribute("cart");

		//合計金額
		int totalPrice = TotalPrice.getAllTotalItemPrice(cart);
		BuyDataBeans cartItem = new BuyDataBeans();
		cartItem.setTotalPrice(totalPrice);
		request.setAttribute("totalPrice", cartItem);

		Date nowDate = new Date();
		Calendar cal = Calendar.getInstance();

		// 翌日
		cal.setTime(nowDate);
		cal.add(Calendar.DAY_OF_MONTH, 1);
		// yyyy-MM-dd形式へ
		String strNextDate = new SimpleDateFormat("MM月dd日",Locale.US).format(cal.getTime());
		request.setAttribute("strNextDate", strNextDate);

		// 翌々日
		cal.setTime(nowDate);
		cal.add(Calendar.DAY_OF_MONTH, 2);
		// yyyy-MM-dd形式へ
		String dateLater2 = new SimpleDateFormat("MM月dd日",Locale.US).format(cal.getTime());
		request.setAttribute("dateLater2", dateLater2);

		// 三日後
		cal.setTime(nowDate);
		cal.add(Calendar.DAY_OF_MONTH, 3);
		// yyyy-MM-dd形式へ
		String dateLater3 = new SimpleDateFormat("MM月dd日",Locale.US).format(cal.getTime());
		request.setAttribute("dateLater3", dateLater3);

		// 四日後
		cal.setTime(nowDate);
		cal.add(Calendar.DAY_OF_MONTH, 4);
		// yyyy-MM-dd形式へ
		String dateLater4 = new SimpleDateFormat("MM月dd日",Locale.US).format(cal.getTime());
		request.setAttribute("dateLater4", dateLater4);

		// 五日後
		cal.setTime(nowDate);
		cal.add(Calendar.DAY_OF_MONTH, 5);
		// yyyy-MM-dd形式へ
		String dateLater5 = new SimpleDateFormat("MM月dd日",Locale.US).format(cal.getTime());
		request.setAttribute("dateLater5", dateLater5);

    	// 購入情報入力画面にフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buyinput.jsp");
		dispatcher.forward(request, response);
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 購入情報入力画面にフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buyinput.jsp");
				dispatcher.forward(request, response);
	}

}
