package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.ItemDataBeans;
import dao.ItemDao;

/**
 * Servlet implementation class ItemMasterDelete
 */
@WebServlet("/ItemMasterDelete")
public class ItemMasterDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemMasterDelete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

        //urlからIDを取得
        String itemId = request.getParameter("id");

        String id = request.getParameter("id");

		// 商品情報を取得
		ItemDao userDao = new ItemDao();
		ItemDataBeans item = userDao.findByItemDetail(itemId);
		request.setAttribute("item", item);

		// 商品マスタ削除画面にフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemmasterdelete.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String id = request.getParameter("id");

		ItemDao itemDao = new ItemDao();

		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		int item = itemDao.ItemDelete(id);

		//正常に登録できた場合、ユーザー画面へ

		//メッセージをセット
		request.setAttribute("msg", "商品ID:[" + id + "]を削除しました");

		// 商品一覧情報を取得
		ArrayList<ItemDataBeans> itemList = itemDao.findAll();
		request.setAttribute("itemList", itemList);

		//1ページに表示する商品数
		int PAGE_MAX_ITEM_COUNT = 10;

		// 検索ワードに対しての総ページ数を取得
		double itemCount = ItemDao.searchItemAllListCount();
		int pageMax = (int) Math.ceil(itemCount / PAGE_MAX_ITEM_COUNT);

		//総アイテム数
		request.setAttribute("itemCount", (int) itemCount);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemmaster.jsp");
		dispatcher.forward(request, response);
	}

}
