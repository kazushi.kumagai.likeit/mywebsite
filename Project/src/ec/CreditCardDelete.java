package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserCreditCardMethodDataBeans;
import beans.UserDataBeans;
import dao.CreditCardDao;
import dao.UserDao;

/**
 * Servlet implementation class CreditCradDelete
 */
@WebServlet("/CreditCardDelete")
public class CreditCardDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreditCardDelete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

        // URLからGETパラメータとしてIDを受け取る
     	String id = request.getParameter("id");

		// クレジットカード情報を取得
		CreditCardDao CreditCardDao = new CreditCardDao();
		UserCreditCardMethodDataBeans creditCard = CreditCardDao.CreditCardDetail(id);
		request.setAttribute("creditcard", creditCard);

		// クレジットカード削除画面にフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/creditcarddelete.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String id = request.getParameter("id");

		// 確認用：idをコンソールに出力
		System.out.println(id);

		int CreditCard = CreditCardDao.Delete(id);

		//データベースエラーが発生した場合
		if (CreditCard == 0) {
			request.setAttribute("errMsg", "削除に失敗しました");
		// ユーザ新規登録画面に戻る
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//正常に登録できた場合、ユーザー画面へ

		//セッションに保存したユーザーIDを取得
		HttpSession session = request.getSession();
		int userId = (int) session.getAttribute("userId");

		//メッセージをセット
		request.setAttribute("msg", "クレジットカードを削除しました");

		//ユーザー情報を取得
		UserDao userDao = new UserDao();
		UserDataBeans userData = userDao.findByUserDetail(userId);
		request.setAttribute("user", userData);

		// クレジットカード情報を取得
		CreditCardDao CreditCardDao = new CreditCardDao();
		ArrayList<UserCreditCardMethodDataBeans> creditCardList = CreditCardDao.findByCreditCardDetail(userId);
		request.setAttribute("creditCardList", creditCardList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user.jsp");
		dispatcher.forward(request, response);
	}

}
