package ec;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import beans.UserCreditCardMethodDataBeans;
import beans.UserDataBeans;
import dao.CreditCardDao;
import dao.UserDao;

/**
 * Servlet implementation class UserPasswordUpdate
 */
@WebServlet("/UserPasswordUpdate")
public class UserPasswordUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserPasswordUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

        //セッションに保存したユーザーIDを取得
        int userId = (int) session.getAttribute("userId");

		// ユーザ一覧情報を取得
		UserDao userDao = new UserDao();
		UserDataBeans user = userDao.findByUserDetail(userId);
		request.setAttribute("user", user);

		// クレジットカード更新画面にフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userpasswordupdate.jsp");
			dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String password_now = request.getParameter("password_now");
		String password_new = request.getParameter("password_new");
		String password_again = request.getParameter("password_again");
		int id = Integer.parseInt(request.getParameter("id"));
		String getPassword = request.getParameter("getPassword");

		// 確認用：idをコンソールに出力
		System.out.println(password_now);
		System.out.println(password_new);
		System.out.println(password_again);
		System.out.println(id);
		System.out.println(getPassword);


		//暗号化処理
		//ハッシュを生成したい元の文字列
			String source = password_now;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
			Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
			String algorithm = "MD5";
		//ハッシュ生成処理
			byte[] bytes = null;
			try {
				bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			} catch (NoSuchAlgorithmException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
			String resultPass_now = DatatypeConverter.printHexBinary(bytes);
		//標準出力
			System.out.println(resultPass_now);

		 //確認用パスワードが一致しなかった、もしくはパスワードが異なる場合
		if (!password_new.equals(password_again)
				|| !resultPass_now.equals(getPassword)) {
			request.setAttribute("errMsg", "パスワードが違います");
			// ユーザ一覧情報を取得
			UserDao userDao = new UserDao();
			UserDataBeans user = userDao.findByUserDetail(id);
			request.setAttribute("user", user);
		// 更新画面に戻る
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userpasswordupdate.jsp");
			dispatcher.forward(request, response);

		//正常に入力できた場合
		}else {
		//パスワードを更新

			//暗号化処理
			//ハッシュを生成したい元の文字列
				String source1 = password_new;
			//ハッシュ生成前にバイト配列に置き換える際のCharset
				Charset charset1 = StandardCharsets.UTF_8;
			//ハッシュアルゴリズム
				String algorithm1 = "MD5";
			//ハッシュ生成処理
				byte[] bytes1 = null;
				try {
					bytes1 = MessageDigest.getInstance(algorithm1).digest(source1.getBytes(charset1));
				} catch (NoSuchAlgorithmException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
				String result = DatatypeConverter.printHexBinary(bytes1);
			//標準出力
				System.out.println(result);

			int UserPassword = UserDao.UserPasswordUpdate(result, id);

		//メッセージをセット
			request.setAttribute("msg", "パスワードを更新しました");

		//ユーザー情報を取得
			UserDao userDao = new UserDao();
			UserDataBeans user = userDao.findByUserDetail(id);
			request.setAttribute("user", user);

		// クレジットカード情報を取得
		CreditCardDao CreditCardDao = new CreditCardDao();
		ArrayList<UserCreditCardMethodDataBeans> creditCardList = CreditCardDao.findByCreditCardDetail(id);
		request.setAttribute("creditCardList", creditCardList);

		//正常に登録できた場合、ユーザー画面へ
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user.jsp");
			dispatcher.forward(request, response);
	}
	}
}
