package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;

/**
 * Servlet implementation class BuyUserRegist
 */
@WebServlet("/BuyUserRegist")
public class BuyUserRegist extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyUserRegist() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// ログインしていない状態での購入ボタンクリックでjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buyuserregist.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String email = request.getParameter("email");
		String password = request.getParameter("password");

//		//暗号化処理
//			//ハッシュを生成したい元の文字列
//				String source = password;
//			//ハッシュ生成前にバイト配列に置き換える際のCharset
//				Charset charset = StandardCharsets.UTF_8;
//			//ハッシュアルゴリズム
//				String algorithm = "MD5";
//			//ハッシュ生成処理
//				byte[] bytes = null;
//				try {
//					bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
//				} catch (NoSuchAlgorithmException e) {
//					// TODO 自動生成された catch ブロック
//					e.printStackTrace();
//				}
//				String result = DatatypeConverter.printHexBinary(bytes);
//			//標準出力
//				System.out.println(result);

		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		int user = UserDao.findByLoginInfo(email, password);

		/** テーブルに該当のデータが見つからなかった場合 **/
		if (user == 0) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "メールアドレスまたはパスワードが異なります");

			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		/** テーブルに該当のデータが見つかった場合 **/
		// セッションにユーザの情報をセット
		HttpSession session = request.getSession();
		session.setAttribute("isLogin", true);
		session.setAttribute("userId", user);

		System.out.println(user);

		// Indexのサーブレットにリダイレクト
		response.sendRedirect("Index");
	}

}
