package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.BuyDetailDataBeans;
import beans.ItemDataBeans;
import dao.BuyDao;
import dao.BuyDetailDao;
import dao.CreditCardDao;
import dao.ItemDao;

/**
 * Servlet implementation class BuyRegist
 */
@WebServlet("/BuyResult")
public class BuyResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyResult() {
        super();
        // TODO Auto-generated constructor stub
    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

        //セッションに保存したユーザーIDを取得
        String userId = String.valueOf(session.getAttribute("userId"));

        //カート情報を取得
      	ArrayList<ItemDataBeans> cart = (ArrayList<ItemDataBeans>) session.getAttribute("cart");

      	// リクエストパラメータの入力項目を取得
        String[] itemId = request.getParameterValues("id");
        String[] amount = request.getParameterValues("amount");
        String totalPrice = request.getParameter("totalPrice");
		String deliveryDay = request.getParameter("deliveryDay");
		String deliveryTime = request.getParameter("deliveryTime");
		String payMethod = request.getParameter("payMethod");
		String deriveryId = request.getParameter("deriveryId");
		String payId = request.getParameter("payId");

		String creditId = request.getParameter("this");

		//購入情報を登録
		int buyId = BuyDao.insertBuy(userId, totalPrice, deriveryId, payId, creditId, deliveryDay, deliveryTime);

		//購入詳細情報を登録
		for (ItemDataBeans item : cart) {
			BuyDetailDataBeans bddb = new BuyDetailDataBeans();
			bddb.setBuyId(buyId);
			bddb.setItemId(item.getId());
			bddb.setAmount(item.getNumber());
			BuyDetailDao.insertBuyDetail(bddb);
		}

		//購入個数情報を登録
		for (ItemDataBeans item : cart) {
			BuyDetailDataBeans bddb = new BuyDetailDataBeans();
			bddb.setItemId(item.getId());
			bddb.setAmount(item.getNumber());
			ItemDao.UpdateItemStock(bddb);
		}

		//クレジットカードが入力された場合
		if(payMethod.equals("クレジットカード") && creditId == null) {
			String company = request.getParameter("company");
			String cardNumber = request.getParameter("cardNumber");
			String name = request.getParameter("name");
			String securityCode = request.getParameter("securityCode");
			String month = request.getParameter("month");
			String year = request.getParameter("year");
			String pay = request.getParameter("pay");
			String payNumber = request.getParameter("payNumber");

			CreditCardDao.insertBuyCreditCard(buyId, userId, company, cardNumber, name, securityCode, month, year, pay, payNumber);

			if(request.getParameter("chek") != null) {
				CreditCardDao.insertCreditCard(userId, company, cardNumber, name, month, year);
			}
		};

		// 購入情報を取得
			BuyDao buyDao = new BuyDao();
			BuyDataBeans buy = buyDao.findByBuyDetail(buyId);
			request.setAttribute("buy", buy);

		//カートを削除
		session.removeAttribute("cart");

		// 購入完了画面にフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buyresult.jsp");
				dispatcher.forward(request, response);
	}

}
