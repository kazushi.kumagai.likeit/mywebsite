package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.DeliveryMethodDataBeans;
import beans.ItemDataBeans;
import beans.PayMethodDataBeans;
import beans.UserCreditCardMethodDataBeans;
import beans.UserDataBeans;
import dao.CreditCardDao;
import dao.DeliveryMethodDao;
import dao.PayMethodDao;
import dao.UserDao;

/**
 * Servlet implementation class BuyConfirm
 */
@WebServlet("/BuyConfirm")
public class BuyConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyConfirm() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

      //カート情報を取得
      	ArrayList<ItemDataBeans> cart = (ArrayList<ItemDataBeans>) session.getAttribute("cart");

      	 //セッションに保存したユーザーIDを取得
        int userId = (int) session.getAttribute("userId");

     // リクエストパラメータの入力項目を取得
		String deliveryDay = request.getParameter("deliveryDay");
		String deliveryTime = request.getParameter("deliveryDate");
		String payMethod = request.getParameter("payMethod");
		String[] itemStatusId = request.getParameterValues("statusId");

		// クレジットカード情報を取得
		CreditCardDao CreditCardDao = new CreditCardDao();
		ArrayList<UserCreditCardMethodDataBeans> creditCardList = CreditCardDao.findByCreditCardDetail(userId);
		request.setAttribute("creditCardList", creditCardList);
		System.out.println(creditCardList);

		String StatusId = null;
		for (String ItemStatusId : itemStatusId) {
				if (ItemStatusId.equals("2")){
				StatusId = "クール便";
				break;
			}else {
				StatusId = "常温";
			}
	}


		request.setAttribute("deliveryDay", deliveryDay);
		request.setAttribute("deliveryTime", deliveryTime);
		request.setAttribute("payMethod", payMethod);

		int deliveryId = 0;
		if(deliveryDay.equals("希望なし") && StatusId.equals("常温")) {
			deliveryId = 1;

		}else if(!deliveryDay.equals("希望なし") && StatusId.equals("常温")) {
			deliveryId = 2;

		}else if(deliveryDay.equals("希望なし") && StatusId.equals("クール便")) {
			deliveryId = 3;

		}else {
			deliveryId = 4;
		}

		System.out.println(deliveryId);

		// ユーザ一覧情報を取得
		UserDao userDao = new UserDao();
		UserDataBeans user = userDao.findByUserDetail(userId);
		request.setAttribute("user", user);

		// 配送情報を取得
		DeliveryMethodDao dmdb = new DeliveryMethodDao();
		DeliveryMethodDataBeans delivery = dmdb.getDeliveryMethodDataBeansByID(deliveryId);
		request.setAttribute("delivery", delivery);

		// 支払い情報を取得
		PayMethodDao pmdb = new PayMethodDao();
		PayMethodDataBeans pay = pmdb.getPayMethodDataBeansByName(payMethod);
		request.setAttribute("pay", pay);


		//合計金額
		int totalPrice = TotalPrice.getAllTotalItemPrice(cart);
		BuyDataBeans cartItem = new BuyDataBeans();
		cartItem.setTotalPrice(totalPrice);
		request.setAttribute("totalPrice", cartItem);

		//消費税
		int tax = TotalPrice.getTax(cart);
		cartItem.setTax(tax);
		request.setAttribute("tax", cartItem);

		// 購入確認画面にフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buyconfirm.jsp");
				dispatcher.forward(request, response);
	}

}
