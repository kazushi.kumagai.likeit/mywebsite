package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDao;

/**
 * Servlet implementation class ItemSearchResult
 */
@WebServlet("/ItemSearchResult")
public class ItemSearchResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemSearchResult() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String searchWord = request.getParameter("searchWord");
		String category = request.getParameter("category");

		if(searchWord == null) {
			searchWord = "";
		}
		// 新たに検索されたキーワードをセッションに格納する
		session.setAttribute("searchWord", searchWord);
		session.setAttribute("category", category);

		//1ページに表示する商品数
		int PAGE_MAX_ITEM_COUNT = 9;

		//表示ページ番号 未指定の場合 1ページ目を表示
		int pageNum = Integer.parseInt(request.getParameter("page_num") == null ? "1" : request.getParameter("page_num"));

		//検査項目から商品を取得
		ItemDao itemDao = new ItemDao();
		ArrayList<ItemDataBeans> itemList = itemDao.searchItemList(searchWord, category, pageNum, PAGE_MAX_ITEM_COUNT);

		// 検索ワードに対しての総ページ数を取得
		double itemCount = ItemDao.searchItemListCount(searchWord, category);
		int pageMax = (int) Math.ceil(itemCount / PAGE_MAX_ITEM_COUNT);

		//総アイテム数
		request.setAttribute("itemCount", (int) itemCount);

		// 総ページ数
		request.setAttribute("pageMax", pageMax);
		// 表示ページ
		request.setAttribute("pageNum", pageNum);
		request.setAttribute("itemList", itemList);

		// 検索結果画面にフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemsearchresult.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
