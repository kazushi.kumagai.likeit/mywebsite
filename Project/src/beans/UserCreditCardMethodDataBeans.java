package beans;

//ユーザークレジットカードデータ

import java.io.Serializable;

public class UserCreditCardMethodDataBeans implements Serializable {
	private int id;
	private int userId;
	private String company;
	private String number;
	private String name;
	private int month;
	private int year;

	//コンストラクタ
	public UserCreditCardMethodDataBeans() {
	}

	//情報を全て取得するためのコンストラクタ
	public UserCreditCardMethodDataBeans(int id, int userId, String company, String number, String name, int month, int year) {
		this.id = id;
		this.userId = userId;
		this.company = company;
		this.number = number;
		this.name = name;
		this.month = month;
		this.year = year;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
}
