package beans;

import java.io.Serializable;
import java.util.Date;

//商品データ

public class ItemDataBeans  implements Serializable {
	private int id;
	private String name;
	private String detail;
	private int price;
	private String fileName;
	private String category;
	private int stock;
	private int statusId;
	private String manufacture;
	private String amount;
	private Date createDate;
	private Date updateDate;

	private String statusName;

	private int number;

	public ItemDataBeans() {
	}

	//全ての情報を取得するコンストラクタ
		public ItemDataBeans(int id, String name, String detail, int price, String fileName, String category, int stock, int statusId, String manufacture, String amount, Date createDate, Date updateDate) {
			this.id = id;
			this.name = name;
			this.detail = detail;
			this.price = price;
			this.fileName = fileName;
			this.category = category;
			this.stock = stock;
			this.statusId = statusId;
			this.manufacture = manufacture;
			this.amount = amount;
			this.createDate = createDate;
			this.updateDate = updateDate;
		}

	//全ての情報を取得するコンストラクタ
	public ItemDataBeans(int id, String name, String detail, int price, String fileName, String category, int stock, int statusId, String manufacture, String amount, Date createDate, Date updateDate, String statusName) {
		this.id = id;
		this.name = name;
		this.detail = detail;
		this.price = price;
		this.fileName = fileName;
		this.category = category;
		this.stock = stock;
		this.statusId = statusId;
		this.manufacture = manufacture;
		this.amount = amount;
		this.createDate = createDate;
		this.updateDate = updateDate;
		this.statusName = statusName;
	}

	//商品を追加するためのコンストラクタ
	public ItemDataBeans(String name, String detail, int price, String fileName, String category, int stock, int statusId, String manufacture, String amount, Date createDate) {
		this.name = name;
		this.detail = detail;
		this.price = price;
		this.fileName = fileName;
		this.category = category;
		this.stock = stock;
		this.statusId = statusId;
		this.manufacture = manufacture;
		this.amount = amount;
		this.createDate = createDate;
	}

	public ItemDataBeans(int id, String name, int price, String fileName, int stock) {
		this.id = id;
		this.name = name;
		this.price = price;
		this.fileName = fileName;
		this.stock = stock;
	}

	public ItemDataBeans(int id, String name, int price, String fileName, int stock, String category, int statusId) {
		this.id = id;
		this.name = name;
		this.price = price;
		this.fileName = fileName;
		this.stock = stock;
		this.category = category;
		this.statusId = statusId;
	}

	public ItemDataBeans(int id, String name, int price, String fileName, int number ,int stock) {
		this.id = id;
		this.name = name;
		this.price = price;
		this.fileName = fileName;
		this.number = number;
		this.stock = stock;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public String getManufacture() {
		return manufacture;
	}

	public void setManufacture(String manufacture) {
		this.manufacture = manufacture;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public int getTaxPrice() {
		int tax = (int) (price * 1.1);
		return tax;
	}
}
