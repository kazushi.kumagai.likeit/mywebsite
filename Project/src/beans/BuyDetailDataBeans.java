package beans;

import java.io.Serializable;

//購入詳細

public class BuyDetailDataBeans  implements Serializable {
	private int id;
	private int buyId;
	private int itemId;
	private int amount;

	//コンストラクタ
	public BuyDetailDataBeans() {
	}

	public BuyDetailDataBeans(int id, String userId, String totalPrice, String deriveryMethodId, String payMethodId,
			String deriveryDate, String deriveryTime, String createDate) {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getBuyId() {
		return buyId;
	}
	public void setBuyId(int buyId) {
		this.buyId = buyId;
	}
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
}