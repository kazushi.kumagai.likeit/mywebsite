package beans;

//購入データ

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BuyDataBeans  implements Serializable {
	private int id;
	private int userId;
	private int totalPrice;
	private int deliveryMethodId;
	private int payMethodId;
	private String deliveryDate;
	private String deliveryTime;
	private Date createDate;

	private String deliveryMethodName;
	private int deliveryMethodPrice;

	private String payMethodName;
	private int payMethodPrice;

	private int tax;

	//コンストラクタ
	public BuyDataBeans() {
	}

	public BuyDataBeans(int id, int userId, int totalPrice, int deliveryMethodId, int  payMethodId, String deliveryDate, String deliveryTime, Date createDate) {
		this.id = id;
		this.userId = userId;
		this.totalPrice = totalPrice;
		this.deliveryMethodId = deliveryMethodId;
		this.payMethodId = payMethodId;
		this.createDate = createDate;
		this.deliveryDate = deliveryDate;
		this.deliveryTime = deliveryTime;

	}

	public BuyDataBeans(int id, int totalPrice, String deliveryMethodName, int deliveryMethodPrice,  String  payMethodName, int  payMethodPrice,  Date createDate) {
		this.id = id;
		this.totalPrice = totalPrice;
		this.deliveryMethodName = deliveryMethodName;
		this.deliveryMethodPrice = deliveryMethodPrice;
		this.payMethodName = payMethodName;
		this.payMethodPrice = payMethodPrice;
		this.createDate = createDate;

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}

	public int getDeliveryMethodId() {
		return deliveryMethodId;
	}

	public void setDeliveryMethodId(int deliveryMethodId) {
		this.deliveryMethodId = deliveryMethodId;
	}

	public int getPayMethodId() {
		return payMethodId;
	}

	public void setPayMethodId(int payMethodId) {
		this.payMethodId = payMethodId;
	}

	public Date getBuyDate() {
		return createDate;
	}

	public String getFormatDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
		return sdf.format(createDate);
	}

	public void setBuyDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getDeliveryMethodName() {
		return deliveryMethodName;
	}

	public void setDeliveryMethodName(String deliveryMethodName) {
		this.deliveryMethodName = deliveryMethodName;
	}

	public int getDeliveryMethodPrice() {
		return deliveryMethodPrice;
	}

	public void setDeliveryMethodPrice(int deliveryMethodPrice) {
		this.deliveryMethodPrice = deliveryMethodPrice;
	}

	public String getPayMethodName() {
		return payMethodName;
	}

	public void setPayMethodName(String payMethodName) {
		this.payMethodName = payMethodName;
	}

	public int getTax() {
		return tax;
	}

	public void setTax(int tax) {
		this.tax = tax;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getDeriverytime() {
		return deliveryTime;
	}

	public void setDeriverytime(String deliveryTime) {
		this.deliveryTime = deliveryTime;
	}

	public int getPayMethodPrice() {
		return payMethodPrice;
	}

	public void setPayMethodPrice(int payMethodPrice) {
		this.payMethodPrice = payMethodPrice;
	}


}
