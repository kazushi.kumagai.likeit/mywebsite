package beans;

//購入データ

import java.io.Serializable;
import java.util.Date;

public class UserDataBeans  implements Serializable {
	private int id;
	private String name;
	private String gender;
	private Date birthDate;
	private String email;
	private String phoneNumber;
	private String postalCode;
	private String prefectures;
	private String address1;
	private String address2;
	private String address3;
	private String password;
	private Date createDate;
	private Date updateDate;

	//コンストラクタ
	public UserDataBeans() {
	}

	// ログインセッションを保存するためのコンストラクタ
	public UserDataBeans(int id) {
		this.id = id;
	}

	// ユーザー情報を全て取得するためのコンストラクタ
		public UserDataBeans(int id, String name, String email, Date birthDate, String phoneNumber, String postalCode, String prefectures, String address1, String address2, String address3, String password, Date createDate, Date updateDate) {
			this.id = id;
			this.name = name;
			this.birthDate = birthDate;
			this.email = email;
			this.phoneNumber = phoneNumber;
			this.postalCode = postalCode;
			this.prefectures = prefectures;
			this.address1 = address1;
			this.address2 = address2;
			this.address3 = address3;
			this.password = password;
			this.createDate = createDate;
			this.updateDate = updateDate;
		}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.gender = name;
	}
	public Date getbirthDate() {
		return birthDate;
	}
	public void setbirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getphoneNumber() {
		return phoneNumber;
	}
	public void setphoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getPrefectures() {
		return prefectures;
	}
	public void setPrefacture(String prefectures) {
		this.prefectures = prefectures;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getAddress3() {
		return address3;
	}
	public void setAddress3(String address3) {
		this.address3 = address3;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}



}