package beans;

import java.io.Serializable;

//決済方法

public class PayMethodDataBeans  implements Serializable {
	private int id;
	private String name;
	private int price;
	
	//コンストラクタ
	public PayMethodDataBeans() {
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
}
